﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Threading;
using GreyMagic;
namespace EloSharp.Managers
{
    public class MemoryManager
    {
        /// <summary>
        ///  ♥ GreyMagic ♥
        /// </summary>
        /// 
        internal static ExternalProcessReader LoL;
        internal static bool Loaded { get; set; }
        internal static Thread MemoryThread { get; set; }
        internal static void Initialize()
        {
            try
            {
                Loaded = false;
                MemoryThread = new Thread(delegate()
                {
                    for (; ; )
                    {
                        if (Loaded) { Thread.Sleep(10); continue; }
                        Thread.Sleep(100);
                        Process[] leagueOfLegends = Process.GetProcessesByName("League of Legends");
                        Debug.Status("Waiting for game to launch...", Color.Orange);
                        if (leagueOfLegends.Length == 0) continue;
                        LoL = new ExternalProcessReader(leagueOfLegends[0]);
                        InjectIfIngame();
                        Loaded = true;
                        LoL.ProcessExited += LoL_ProcessExited;
                        if (GameState != GameState.Loading) continue;
                        //SkinPickerGUI.InitSkinPlayerStructs();
                        //SkinPickerGUI.ApplyHook();
                        Debug.Log("In loading screen...", MessageTypes.Info);
                        ScriptManager.Events.OnLoLLoading();
                        //SkinPickerGUI.Listen();
                    }
                // ReSharper disable once FunctionNeverReturns
                });
                MemoryThread.Start();
            }
            catch (Exception ex)
            {
                Debug.Log("Exception thrown when initializing memory. Error:" + ex.Message, MessageTypes.Error);
            }
        }
        internal static void InjectIfIngame()
        {
            new Thread(delegate()
            {
                Debug.Status("Injected!", Color.Green);
                do
                {
                    var g = GameState;
                    Console.WriteLine(g);
                    Thread.Sleep(10);
                } while (GameState != GameState.InGame);
                Debug.Log(string.Format("Attached to League of Legends with PID: {0}", LoL.Process.Id), MessageTypes.Success);
                ObjectManager.Initialize();
                int ticks = 0;
                do
                {
                    if (ticks > 50)
                    {
                        Debug.Log("Waited over 50 ticks for the ObjectManager to become filled. Try to restart EloSharp.", MessageTypes.Critical);
                        Debug.Log("Startup procedure failed.", MessageTypes.Critical);
                        break;
                    }
                    Thread.Sleep(5);
                    ticks++;
                } while (ObjectManager.Me == null);
                //if (ticks < 50)
                //    SpellManager.Initialize();
            }).Start();
        }
        static void LoL_ProcessExited(object sender, EventArgs e)
        {
            if (ObjectManager.WorkerThread != null)
                ObjectManager.WorkerThread.Abort();
            ObjectManager.Initialize();
            Debug.Log("Game over! League of Legends closed", MessageTypes.Info);
            Loaded = false;
        }
        /// <summary>
        /// 
        /// </summary>
        public static bool IsInLoadingScreen
        {
            get
            {
                return ((int)Base.Read<IntPtr>((IntPtr)Offsets.LoL.LocalPlayer) == 0) && GameState == GameState.Loading;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public static GameState GameState
        {
            get
            {
                try
                {
                    var value = Base.Read<byte>((IntPtr)Offsets.LoL.GameState, true);
                    return (GameState) value;
                }
                catch (Exception)
                {
                    return GameState.GameExiting;
                }
            }
        }
        /// <summary>
        /// Returns whether GreyMagic is launched or not.
        /// </summary>
        public static bool IsInitialized
        {
            get
            {
                try
                {
                    var leagueOfLegends = Process.GetProcessesByName("League of Legends");
                    if (leagueOfLegends.Length == 0)
                    {
                        return false;
                    }
                    if (leagueOfLegends[0].Id == LoL.Process.Id)
                    {
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Console.Write(ex.ToString());
                }
                return false;
            }
        }
        public class Base
        {
            public static T Read<T>(IntPtr address, bool value = false) where T : struct
            {
                try
                {
                    return LoL.Read<T>(address, value);
                }
                catch (Exception ex)
                {
                    Debug.Log(string.Format("Memory exception: {0}", ex.Message), MessageTypes.Critical);
                    return default(T);
                }
            }
            public static string ReadString(IntPtr address, Encoding encoding)
            {
                try
                {
                    return LoL.ReadString(address, encoding);
                }
                catch (Exception ex)
                {
                    Debug.Log(string.Format("Memory exception: {0}", ex.Message), MessageTypes.Critical);
                    return null;
                }
            }
        }
    }
}
