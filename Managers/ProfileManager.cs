﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
namespace EloSharp.Managers
{
    class ProfileManager
    {
        internal class Profile
        {
            [JsonProperty("Name")]
            public string Name { get; set; }
            [JsonProperty("ConnectedScripts")]
            public List<string> ConnectedScripts = new List<string>();
        }
        internal static List<Profile> ProfileList = new List<Profile>();
        internal static Profile ActiveProfile
        {
            get
            {
                return ProfileList.FirstOrDefault(o => o.Name == Program.MainForm.Dashboard.Cmb_Profiles.Text);
            }
        }
        internal static Profile GetActiveProfile(string profileName)
        {
            return ProfileList.FirstOrDefault(o => o.Name == profileName);
        }
        internal static void Initialize()
        {
            try
            {
                var list = new JavaScriptSerializer().Deserialize<List<Profile>>(SettingsManager.ReadProperty("ProfileList"));
                if (list != null)
                    ProfileList = new JavaScriptSerializer().Deserialize<List<Profile>>(SettingsManager.ReadProperty("ProfileList"));

                RefreshGui();
            }
            catch (Exception ex)
            {
                Debug.Log(ex.Message, MessageTypes.Critical);
            }
        }
        internal static void LoadProfile(string profileName)
        {
            Program.MainForm.Dashboard.DashboardScriptsTab.ScriptControl.Controls.Clear();
        }
        internal static void RefreshGui()
        {
            Program.MainForm.Dashboard.Cmb_Profiles.Items.Clear();
            foreach (Profile profile in ProfileList)
            {
                Program.MainForm.Dashboard.Cmb_Profiles.Items.Add(profile.Name);
            }
        }
        internal static void InstallScript(ScriptManager.Script script)
        {
            if (ActiveProfile == null) return;
            ActiveProfile.ConnectedScripts.Add(script.Hash);
            Save();
        }
        internal static void UninstallScript(ScriptManager.Script script)
        {
            if (ActiveProfile == null) return;
            ActiveProfile.ConnectedScripts.Remove(script.Hash);
            Save();
            Program.MainForm.Dashboard.DashboardScriptsTab.DisplayScripts();
        }
        internal static void Create(string name)
        {
            ProfileList.Add(new Profile { Name = name});
            Save();
            RefreshGui();
        }
        internal static void Save()
        {
            SettingsManager.WriteProperty("ProfileList", ProfileList);
        }
        internal static void Remove(Profile profile)
        {
            foreach (var scriptHash in profile.ConnectedScripts)
            {
                ScriptManager.Deactivate(scriptHash);
            }
            ProfileList.Remove(profile);
            Save();
            RefreshGui();
        }
    }
}