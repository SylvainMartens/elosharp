﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using EloSharp.LoLInternals;
using EloSharp.Scripts;
using EloSharp.Utils;
using Microsoft.CSharp;
namespace EloSharp.Managers
{
    class ScriptManager
    {
        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();
        internal class Script
        {
            public string Name { get; set; }
            public string Hash { get; set; }
            public bool Activated { get; set; }
            public string Author { get; set; }
            public Version Version { get; set; }
            public FileInfo Path { get; set; }
            public FileInfo DllPath { get; set; }
            public List<Thread> ThreadList { get; set; }
            public ScriptInterface Interface;
            public List<string> Files { get; set; }
            public string Directory { get; set; }
            public ProfileManager.Profile Profile { get; set; }
        }
        internal static List<Script> LoadedScripts = new List<Script>();
        internal static bool IsLoading { get; set; }

        internal static void CompileFolder(string folder, ProfileManager.Profile profile = null)
        {
            var dir = new DirectoryInfo(folder);
            string dllPath = string.Format("{0}/{1}.dll", dir.FullName, dir.Name);
            Debug.Log(string.Format("Loading {0}...", dir.Name.Replace(".cs", string.Empty)), MessageTypes.Info);
            var cscp = new CSharpCodeProvider(new Dictionary<string, string> { { "CompilerVersion", "v4.0" } });
            var args = new CompilerParameters(new[] { 
                "mscorlib.dll", "System.Core.dll", Application.ExecutablePath, "System.Drawing.dll", "System.Windows.Forms.dll", "System.dll", "System.Data.dll", "FlatTheme.dll", "FastColoredTextBox.dll" },
                dllPath,
                true
            )
            {
                GenerateExecutable = false,
                TreatWarningsAsErrors = false
            };
            var sources = new List<string>();
            var files = new List<string>();
            var sourceDirectory = new DirectoryInfo(dir.FullName);
            foreach (FileInfo sourcefile in sourceDirectory.GetFiles("*.cs", SearchOption.AllDirectories))
            {
                string content = File.ReadAllText(sourcefile.FullName);
                sources.Add(content);
                files.Add(sourcefile.FullName);
            }
            CompilerResults cr = cscp.CompileAssemblyFromSource(args, sources.ToArray());
            if (cr.Errors.Count > 0)
            {
                Debug.Log(string.Format("Script {0} is invalid. Contact creator of the script.", dir.Name.Replace(".cs", string.Empty)), MessageTypes.Error);
                foreach (CompilerError err in cr.Errors)
                {
                    Debug.Log(string.Format("Error: {0} Line: {1}", err.ErrorText, err.Line), MessageTypes.Error);
                }
            }
            else
            {
                bool hasErrors = false;
                try
                {
                    Assembly assembly = Assembly.LoadFrom(dllPath);
                    Type entryType = null;
                    foreach (Type type in assembly.GetTypes())
                    {
                        foreach (MethodInfo method in type.GetMethods())
                            if (method.Name == "get_Author")
                            {
                                entryType = type;
                            }
                    }
                    if (entryType == null)
                    {
                        Debug.Log(string.Format("Script: {0} is missing basic interface types (Author, Version, ..)", assembly.FullName), MessageTypes.Error);
                        return;
                    }
                    var ebInterface = (ScriptInterface)Activator.CreateInstance(entryType);
                    LoadedScripts.Add(new Script
                    {
                        Name = ebInterface.Name,
                        Hash = Crypto.Encode(new FileInfo(dllPath).CreationTimeUtc + ebInterface.Name),
                        Interface = ebInterface,
                        Activated = false,
                        Directory = folder,
                        Author = ebInterface.Author,
                        Version = ebInterface.Version,
                        DllPath = new FileInfo(dllPath),
                        Files = files,
                        Profile = profile ?? new ProfileManager.Profile()
                    });
                }
                catch (Exception ex)
                {
                    hasErrors = true;
                    Debug.Log(string.Format("{0}", ex.Message), MessageTypes.Error);
                    Debug.Log(string.Format("Failed to load script {0}!", dir.Name), MessageTypes.Error);
                }
                finally
                {
                    if (!hasErrors)
                        Debug.Log(string.Format("{0} loaded!", dir.Name), MessageTypes.Success);
                }
            }
        }

        internal static void Compile(string specificDir = null, ProfileManager.Profile profile = null)
        {
            try
            {
                lock (LoadedScripts)
                {
                    IsLoading = true;
                    LoadedScripts.Clear();
                    if (specificDir == null)
                    {
                        Directory.CreateDirectory(string.Format("{0}/Scripts/", Application.StartupPath));
                        foreach (string dir in Directory.GetDirectories(Application.StartupPath + "/Scripts/"))
                        {
                            CompileFolder(dir);
                        }
                    }
                    else
                    {
                        CompileFolder(specificDir);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Log("Compiler error! Try to delete all scripts.", MessageTypes.Error);
                Debug.Log(string.Format("{0}", ex.Message), MessageTypes.Error);
            }
            Debug.Log(
                LoadedScripts.Count == 0
                    ? "0 scripts loaded. Why don't you download some? www.elobuddy.net"
                    : string.Format("{0} scripts loaded", LoadedScripts.Count), MessageTypes.Info);
            IsLoading = false;
        }
        internal static void Activate(string hash)
        {
            Script activeScript = LoadedScripts.FirstOrDefault(o => o.Hash == hash);
            if (activeScript == null) return;
            activeScript.Activated = true;
            Debug.Log(string.Format("{0} activated!", activeScript.Name), MessageTypes.Info);
            Events.OnLoad(activeScript.Name);
        }
        internal static void Deactivate(string hash)
        {
            Script activeScript = LoadedScripts.FirstOrDefault(o => o.Hash == hash);
            if (activeScript == null) return;
            activeScript.Activated = false;
            Events.OnUnload(activeScript);
            Debug.Log(string.Format("{0} deactivated!", activeScript.Name), MessageTypes.Info);
        }
        internal static void Delete(Script script)
        {
            Deactivate(script.Hash);
            foreach (string file in script.Files)
            {
                File.Delete(file);
            }
            Debug.Log(string.Format("{0} deleted!", script.Name), MessageTypes.Info);
        }
        internal static void CallEventAction(Script script, string Event)
        {
            try
            {
                Script activeScript = LoadedScripts.FirstOrDefault(o => o.Name == script.Name);
                if (activeScript != null && activeScript.ThreadList == null)
                    activeScript.ThreadList = new List<Thread>();
                if (Event == "OnUnload")
                {
                    //Kill all threads
                    if (activeScript != null)
                        foreach (Thread thread in activeScript.ThreadList)
                        {
                            thread.Abort();
                        }
                }
                var tmp = new Thread(delegate()
                {
                    try
                    {
                        if (activeScript != null)
                        {
                            MethodInfo mi = activeScript.Interface.GetType().GetMethod(Event);
                            if (mi != null)
                                mi.Invoke(activeScript.Interface, null);
                        }
                    }
                    catch (ThreadAbortException) { }
                    catch (Exception ex)
                    {
                        Debug.Log(string.Format("[{0}] {1}", script.Name, ex.Message), MessageTypes.Error);
                        if (ex.InnerException != null)
                            Debug.Log(string.Format("[{0}] {1}", script.Name, ex.InnerException.Message), MessageTypes.Error);

                    }
                });
                tmp.Start();
                if (activeScript != null) activeScript.ThreadList.Add(tmp);
            }
            catch (Exception ex)
            {
                Debug.Log(string.Format("{0}", ex.Message), MessageTypes.Error);
            }
        }
        internal static void CallEventActionWithParameters(Script script, string Event, object[] args)
        {
            Script activeScript = LoadedScripts.FirstOrDefault(o => o.Name == script.Name);
            if (activeScript == null) return;
            if (activeScript.ThreadList == null)
                activeScript.ThreadList = new List<Thread>();
            var tmp = new Thread(delegate()
            {
                try
                {
                    MethodInfo mi = activeScript.Interface.GetType().GetMethod(Event);
                    if (mi != null)
                        mi.Invoke(activeScript.Interface, args);

                }
                catch (ThreadAbortException) { }
                catch (Exception ex)
                {
                    Debug.Log(string.Format("[{0}] {1}", script.Name, ex.Message), MessageTypes.Error);
                    if (ex.InnerException != null)
                        Debug.Log(string.Format("[{0}] {1}", script.Name, ex.InnerException.Message), MessageTypes.Error);

                }
            });
            tmp.Start();
            activeScript.ThreadList.Add(tmp);
        }
        public class Events
        {
            internal static void OnLoad(string name = "")
            {
                if (name == null) return;

                foreach (var script in LoadedScripts.Where(o => o.Activated).Where(script => name.Length <= 0 || name == script.Name))
                {
                    CallEventAction(script, "OnLoad");
                }
            }
            internal static void OnUnload(Script script)
            {
                CallEventAction(script, "OnUnload");
            }
            internal static void OnPulse()
            {
                if (!MemoryManager.IsInitialized) return;
                if (MemoryManager.GameState != GameState.InGame) return;

                foreach (Script script in LoadedScripts.Where(o => o.Activated))
                {
                    CallEventAction(script, "OnPulse");
                }
            }
            internal static void OnLoLLoading()
            {
                foreach (Script script in LoadedScripts.Where(o => o.Activated))
                {
                    CallEventAction(script, "OnLoLLoading");
                }
            }
            internal static void OnKeyDown(object sender, KeyEventArgs e)
            {
                if (!MemoryManager.IsInitialized) return;
                if (GetForegroundWindow() != MemoryManager.LoL.Process.MainWindowHandle) return;
                if (Game.IsChatOpen) return;
                foreach (Script script in LoadedScripts.Where(o => o.Activated))
                {
                    CallEventActionWithParameters(script, "OnKeyDown", new object[] { e.KeyCode });
                }
            }
        }
    }
}
