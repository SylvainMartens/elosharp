﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EloSharp.Managers
{
    public class SettingsManager
    {
        private class Setting
        {
            public string Property { get; set; }
            public object Value { get; set; }
        }
        private static FileInfo SettingsFile { get { return new FileInfo(string.Format("{0}/EloSharp/Settings.elosharp", Environment.GetEnvironmentVariable("AppData"))); } }
        private static readonly List<Setting> SettingsList = new List<Setting>();
        private static bool Prepared { get; set; }
        internal static void Prepare()
        {
            try
            {
                foreach (Setting setting in JsonConvert.DeserializeObject<Setting[]>(File.ReadAllText(SettingsFile.FullName)))
                {
                    SettingsList.Add(setting);
                }
            }
            catch (Exception) //Someone broke the .buddy file / it's empty
            {
                File.WriteAllText(SettingsFile.FullName, string.Empty);
            }
            Prepared = true;
        }
        internal static void Save()
        {
            JArray o = JArray.FromObject(SettingsList);
            File.WriteAllText(SettingsFile.FullName, o.ToString());
        }
        public static void WriteProperty(string property, object value)
        {
            if (!Prepared) Prepare();
            Setting setting = SettingsList.FirstOrDefault(o => o.Property == property);
            if (setting != null)
            {
                SettingsList.Remove(setting);
                setting.Value = value;
                SettingsList.Add(setting);
            }
            else
            {
                SettingsList.Add(new Setting { Property = property, Value = value });
            }
            Save();
        }
        public static void WriteProperty(string property, string value)
        {
            if (!Prepared) Prepare();
            Setting setting = SettingsList.FirstOrDefault(o => o.Property == property);
            if (setting != null)
            {
                SettingsList.Remove(setting);
                setting.Value = value;
                SettingsList.Add(setting);
            }
            else
            {
                SettingsList.Add(new Setting { Property = property, Value = value });
            }
            Save();
        }
        public static string ReadProperty(string property)
        {
            if (!Prepared) Prepare();
            try
            {
                var firstOrDefault = SettingsList.FirstOrDefault(o => o.Property == property);
                if (firstOrDefault != null)
                {
                    object value = firstOrDefault.Value;
                    return value.ToString();
                }
            }
            catch (Exception)
            {
                return "";
            }
            return "";
        }
        public static T ReadProperty<T>(string property)
        {
// ReSharper disable once ReplaceWithSingleCallToFirstOrDefault
// ReSharper disable once PossibleNullReferenceException
            return (T)SettingsList.Where(o => o.Property == property).FirstOrDefault().Value;
        }
    }
}
