﻿using System;
using System.Collections.Generic;
using EloSharp.LoLInternals;
namespace EloSharp.Managers
{
    public class SpellManager
    {
        public static List<Spell> SpellList = new List<Spell>();
        public static void Initialize()
        {
            if (!MemoryManager.IsInitialized) { Debug.Log("Error: Tried to update SpellBook, but Memory not initialized yet.", MessageTypes.Error); return; }
            if (MemoryManager.GameState != GameState.InGame) { Debug.Log("Error: Tried to update SpellBook, but not ingame yet.", MessageTypes.Error); return; }
            if (ObjectManager.Me == null) { Debug.Log("Error: Tried to update SpellBook, but ObjectManager not fired yet.", MessageTypes.Critical); return; }

            lock (SpellList)
            {
                Debug.Log("Loading Spellbook...", MessageTypes.Info);
                SpellList.Clear();
                var localPlayer = MemoryManager.Base.Read<IntPtr>((IntPtr)Offsets.LoL.LocalPlayer);
                var championSci = localPlayer + Offsets.SpellInfo.ChampionSci;
                var summonerSci = localPlayer + Offsets.SpellInfo.SummonerSci;
                for (var i = 0; i <= 3; i++)
                {
                    var Base = MemoryManager.Base.Read<IntPtr>(championSci + (i * 4) + Offsets.SpellInfo.SpellInfoPointer);
                    var tmp = new Spell(Base, i, false);
                    SpellList.Add(tmp);
                }
                for (var i = 0; i <= 1; i++)
                {
                    var Base = MemoryManager.Base.Read<IntPtr>(summonerSci + (i * 4) + Offsets.SpellInfo.SpellInfoPointer);
                    var tmp = new Spell(Base, i, true);

                    SpellList.Add(tmp);
                }
            }
            foreach (var spell in SpellList)
            {
                Debug.Log(string.Format("Spell '{0}' added to spellbook -> {1}", spell.Name, spell.SpellState), MessageTypes.Success);
            }
            Debug.Log("Spellbook loaded", MessageTypes.Info);
        }
        internal static void UpgradeSpell(int slot)
        {
            if (!MemoryManager.IsInitialized) { Debug.Log("Error: Tried to upgrade a spell, but Memory not initialized yet.", MessageTypes.Error); return; }
            if (MemoryManager.GameState != GameState.InGame) { Debug.Log("Error: Tried to upgrade a spell, but not ingame yet.", MessageTypes.Error); return; }
            if (slot > 3)
            {
                Debug.Log("What the fuck are you doing?");
                return;
            }
            using (var executor = new Executor())
            {
                executor.AddLine("mov eax, {0}", ObjectManager.Me.BaseAddress + Offsets.SpellInfo.ChampionSci);
                executor.AddLine("push {0}", slot);
                executor.AddLine("call {0}", Offsets.LoL.UpgradeSpell);
                executor.AddLine("ret");
                executor.Execute();
            }
        }
        internal static void LearnSpell(int slot)
        {
            UpgradeSpell(slot);
        }
        internal static int SpellPoints
        {
            get { return MemoryManager.Base.Read<int>((IntPtr)MemoryManager.Base.Read<int>(ObjectManager.Me.BaseAddress + Offsets.SpellInfo.SkillPoints)); }
        }
    }
}
