﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using EloSharp.LoLInternals.Objects;
namespace EloSharp.Managers
{
    public class ObjectManager
    {
        public static ConcurrentBag<GameObject> GameObjects = new ConcurrentBag<GameObject>();
        public static ConcurrentBag<Player> PlayerList = new ConcurrentBag<Player>();
        public static ConcurrentBag<Turret> TurretList = new ConcurrentBag<Turret>();
        public static ConcurrentBag<Ward> WardList = new ConcurrentBag<Ward>();
        public static ConcurrentBag<Minion> MinionList = new ConcurrentBag<Minion>();
        public static Player Me { get; set; }
        internal static Thread WorkerThread { get; set; }
        internal static void Initialize()
        {
            WorkerThread = new Thread(delegate()
            {
                do
                {
                    Refresh();
                    Thread.Sleep(1000 / 100);
                } while (MemoryManager.GameState == GameState.InGame);

                do
                {
                    Thread.Sleep(25); //Wait till LoL is launched again
                } while (!MemoryManager.IsInitialized);
            });
            WorkerThread.Start();
        }
        internal static int PulseCount { get; set; }
        public static void Refresh()
        {
            if (!MemoryManager.IsInitialized) return;
            if (MemoryManager.GameState != GameState.InGame) return;
            Me = new Player((IntPtr)Offsets.LoL.LocalPlayer);
            //Thanks for Items.Clear(), C#.
            ScriptManager.Events.OnPulse();
            PulseCount = 0;
            return;

            GameObjects = new ConcurrentBag<GameObject>();
            PlayerList = new ConcurrentBag<Player>();
            TurretList = new ConcurrentBag<Turret>();
            WardList = new ConcurrentBag<Ward>();
            MinionList = new ConcurrentBag<Minion>();
            var firstObject = MemoryManager.Base.Read<IntPtr>((IntPtr)Offsets.ObjectManager.FirstObject);
            var lastObject = MemoryManager.Base.Read<IntPtr>((IntPtr)Offsets.ObjectManager.LastObject);
            var curObj = firstObject + 0x4;
            lock (GameObjects)
            {
                while (curObj != lastObject && MemoryManager.GameState == GameState.InGame)
                {
                    //if (curObj == null) continue;
                    IntPtr nextObj = curObj + 0x4;

                    var gameObject = new GameObject(curObj);
                    GameObjects.Add(gameObject);
                    switch (gameObject.ObjectType)
                    {
                        case ObjectType.Player:
                            PlayerList.Add(new Player(curObj));
                        break;
                        case ObjectType.Turret:
                            TurretList.Add(new Turret(curObj));
                        break;
                        case ObjectType.VisionWard:
                            WardList.Add(new Ward(curObj));
                        break;
                        case ObjectType.SightWard:
                            WardList.Add(new Ward(curObj));
                        break;
                        case ObjectType.Minion:
                            MinionList.Add(new Minion(curObj));
                        break;
                        case ObjectType.Creeps:
                            MinionList.Add(new Minion(curObj));
                        break;
                    }
                    curObj = nextObj;
                }
            }
            if (PulseCount++%13 != 0) return;
            ScriptManager.Events.OnPulse();
            PulseCount = 0;
        }
    }
}
