﻿using EloSharp.UserControls;
namespace EloSharp.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TopBar = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.CloseForm = new FlatTheme.FlatClose();
            this.MinimizeForm = new FlatTheme.FlatMini();
            this.Tab_Log = new System.Windows.Forms.Panel();
            this.Panel_Tab4Top = new System.Windows.Forms.Panel();
            this.Tab4_LabelSubTitle = new System.Windows.Forms.Label();
            this.Tab4_LabelTitle = new System.Windows.Forms.Label();
            this.Tab_DeveloperTools = new System.Windows.Forms.Panel();
            this.Panel_Tab3Top = new System.Windows.Forms.Panel();
            this.Tab3_LabelSubTitle = new System.Windows.Forms.Label();
            this.Tab3_LabelTitle = new System.Windows.Forms.Label();
            this.Tab_Downloader = new System.Windows.Forms.Panel();
            this.Tab2_LabelTitle = new System.Windows.Forms.Label();
            this.Tab2_LabelSubTitle = new System.Windows.Forms.Label();
            this.Panel_Tab2Top = new System.Windows.Forms.Panel();
            this.Tab_Dashboard = new System.Windows.Forms.Panel();
            this.Tab1_LabelSubTitle = new System.Windows.Forms.Label();
            this.Tab1_LabelTitle = new System.Windows.Forms.Label();
            this.Panel_Tab1Top = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel_bottom_black_line = new System.Windows.Forms.Panel();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.StatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.downloadManager = new EloSharp.UserControls.DownloadManager();
            this.Dashboard = new EloSharp.UserControls.Dashboard();
            this.developerTools = new EloSharp.UserControls.DeveloperTools();
            this.ConsoleLog = new EloSharp.UserControls.ConsoleLog();
            this.TopBar.SuspendLayout();
            this.Tab_Log.SuspendLayout();
            this.Tab_DeveloperTools.SuspendLayout();
            this.Tab_Downloader.SuspendLayout();
            this.Tab_Dashboard.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // TopBar
            // 
            this.TopBar.BackColor = System.Drawing.Color.Transparent;
            this.TopBar.Controls.Add(this.label3);
            this.TopBar.Controls.Add(this.CloseForm);
            this.TopBar.Controls.Add(this.MinimizeForm);
            this.TopBar.Controls.Add(this.Tab_Log);
            this.TopBar.Controls.Add(this.Tab_DeveloperTools);
            this.TopBar.Controls.Add(this.Tab_Downloader);
            this.TopBar.Controls.Add(this.Tab_Dashboard);
            this.TopBar.Controls.Add(this.label2);
            this.TopBar.Controls.Add(this.label1);
            this.TopBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopBar.Location = new System.Drawing.Point(0, 0);
            this.TopBar.Name = "TopBar";
            this.TopBar.Size = new System.Drawing.Size(796, 81);
            this.TopBar.TabIndex = 7;
            this.TopBar.DoubleClick += new System.EventHandler(this.TopBar_DoubleClick);
            this.TopBar.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Impact", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(25, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 19);
            this.label3.TabIndex = 15;
            this.label3.Text = "By Hesa";
            this.label3.DoubleClick += new System.EventHandler(this.TopBar_DoubleClick);
            this.label3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
            // 
            // CloseForm
            // 
            this.CloseForm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CloseForm.BackColor = System.Drawing.Color.White;
            this.CloseForm.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.CloseForm.Font = new System.Drawing.Font("Marlett", 10F);
            this.CloseForm.Location = new System.Drawing.Point(772, 6);
            this.CloseForm.Name = "CloseForm";
            this.CloseForm.Size = new System.Drawing.Size(18, 18);
            this.CloseForm.TabIndex = 12;
            this.CloseForm.Text = "flatClose1";
            this.CloseForm.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.CloseForm.Click += new System.EventHandler(this.CloseForm_Click);
            // 
            // MinimizeForm
            // 
            this.MinimizeForm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MinimizeForm.BackColor = System.Drawing.Color.White;
            this.MinimizeForm.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(49)))));
            this.MinimizeForm.Font = new System.Drawing.Font("Marlett", 12F);
            this.MinimizeForm.Location = new System.Drawing.Point(748, 6);
            this.MinimizeForm.Name = "MinimizeForm";
            this.MinimizeForm.Size = new System.Drawing.Size(18, 18);
            this.MinimizeForm.TabIndex = 11;
            this.MinimizeForm.Text = "flatMini1";
            this.MinimizeForm.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            // 
            // Tab_Log
            // 
            this.Tab_Log.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Tab_Log.BackColor = System.Drawing.Color.Transparent;
            this.Tab_Log.Controls.Add(this.Panel_Tab4Top);
            this.Tab_Log.Controls.Add(this.Tab4_LabelSubTitle);
            this.Tab_Log.Controls.Add(this.Tab4_LabelTitle);
            this.Tab_Log.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Tab_Log.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Tab_Log.Location = new System.Drawing.Point(602, 6);
            this.Tab_Log.Name = "Tab_Log";
            this.Tab_Log.Size = new System.Drawing.Size(131, 66);
            this.Tab_Log.TabIndex = 5;
            this.Tab_Log.Click += new System.EventHandler(this.Tab_Log_Click);
            // 
            // Panel_Tab4Top
            // 
            this.Panel_Tab4Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.Panel_Tab4Top.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Tab4Top.Location = new System.Drawing.Point(0, 0);
            this.Panel_Tab4Top.Name = "Panel_Tab4Top";
            this.Panel_Tab4Top.Size = new System.Drawing.Size(131, 10);
            this.Panel_Tab4Top.TabIndex = 13;
            this.Panel_Tab4Top.Click += new System.EventHandler(this.Tab_Log_Click);
            // 
            // Tab4_LabelSubTitle
            // 
            this.Tab4_LabelSubTitle.AutoSize = true;
            this.Tab4_LabelSubTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.Tab4_LabelSubTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.Tab4_LabelSubTitle.Location = new System.Drawing.Point(18, 39);
            this.Tab4_LabelSubTitle.Name = "Tab4_LabelSubTitle";
            this.Tab4_LabelSubTitle.Size = new System.Drawing.Size(98, 15);
            this.Tab4_LabelSubTitle.TabIndex = 12;
            this.Tab4_LabelSubTitle.Text = "debugging is fun";
            this.Tab4_LabelSubTitle.Click += new System.EventHandler(this.Tab_Log_Click);
            // 
            // Tab4_LabelTitle
            // 
            this.Tab4_LabelTitle.AutoSize = true;
            this.Tab4_LabelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.Tab4_LabelTitle.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Tab4_LabelTitle.Location = new System.Drawing.Point(47, 18);
            this.Tab4_LabelTitle.Name = "Tab4_LabelTitle";
            this.Tab4_LabelTitle.Size = new System.Drawing.Size(36, 18);
            this.Tab4_LabelTitle.TabIndex = 11;
            this.Tab4_LabelTitle.Text = "Log";
            this.Tab4_LabelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Tab4_LabelTitle.Click += new System.EventHandler(this.Tab_Log_Click);
            // 
            // Tab_DeveloperTools
            // 
            this.Tab_DeveloperTools.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Tab_DeveloperTools.BackColor = System.Drawing.Color.Transparent;
            this.Tab_DeveloperTools.Controls.Add(this.Panel_Tab3Top);
            this.Tab_DeveloperTools.Controls.Add(this.Tab3_LabelSubTitle);
            this.Tab_DeveloperTools.Controls.Add(this.Tab3_LabelTitle);
            this.Tab_DeveloperTools.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Tab_DeveloperTools.Location = new System.Drawing.Point(465, 6);
            this.Tab_DeveloperTools.Name = "Tab_DeveloperTools";
            this.Tab_DeveloperTools.Size = new System.Drawing.Size(131, 66);
            this.Tab_DeveloperTools.TabIndex = 10;
            this.Tab_DeveloperTools.Click += new System.EventHandler(this.Tab_DeveloperTools_Click);
            // 
            // Panel_Tab3Top
            // 
            this.Panel_Tab3Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.Panel_Tab3Top.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Tab3Top.Location = new System.Drawing.Point(0, 0);
            this.Panel_Tab3Top.Name = "Panel_Tab3Top";
            this.Panel_Tab3Top.Size = new System.Drawing.Size(131, 10);
            this.Panel_Tab3Top.TabIndex = 13;
            this.Panel_Tab3Top.Click += new System.EventHandler(this.Tab_DeveloperTools_Click);
            // 
            // Tab3_LabelSubTitle
            // 
            this.Tab3_LabelSubTitle.AutoSize = true;
            this.Tab3_LabelSubTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.Tab3_LabelSubTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.Tab3_LabelSubTitle.Location = new System.Drawing.Point(13, 39);
            this.Tab3_LabelSubTitle.Name = "Tab3_LabelSubTitle";
            this.Tab3_LabelSubTitle.Size = new System.Drawing.Size(106, 15);
            this.Tab3_LabelSubTitle.TabIndex = 12;
            this.Tab3_LabelSubTitle.Text = "Debugger && Tools";
            this.Tab3_LabelSubTitle.Click += new System.EventHandler(this.Tab_DeveloperTools_Click);
            // 
            // Tab3_LabelTitle
            // 
            this.Tab3_LabelTitle.AutoSize = true;
            this.Tab3_LabelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.Tab3_LabelTitle.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Tab3_LabelTitle.Location = new System.Drawing.Point(0, 18);
            this.Tab3_LabelTitle.Name = "Tab3_LabelTitle";
            this.Tab3_LabelTitle.Size = new System.Drawing.Size(132, 18);
            this.Tab3_LabelTitle.TabIndex = 11;
            this.Tab3_LabelTitle.Text = "Developer Tools";
            this.Tab3_LabelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Tab3_LabelTitle.Click += new System.EventHandler(this.Tab_DeveloperTools_Click);
            // 
            // Tab_Downloader
            // 
            this.Tab_Downloader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Tab_Downloader.BackColor = System.Drawing.Color.Transparent;
            this.Tab_Downloader.Controls.Add(this.Tab2_LabelTitle);
            this.Tab_Downloader.Controls.Add(this.Tab2_LabelSubTitle);
            this.Tab_Downloader.Controls.Add(this.Panel_Tab2Top);
            this.Tab_Downloader.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Tab_Downloader.Location = new System.Drawing.Point(328, 6);
            this.Tab_Downloader.Name = "Tab_Downloader";
            this.Tab_Downloader.Size = new System.Drawing.Size(131, 66);
            this.Tab_Downloader.TabIndex = 4;
            this.Tab_Downloader.Click += new System.EventHandler(this.Tab_Downloader_Click);
            // 
            // Tab2_LabelTitle
            // 
            this.Tab2_LabelTitle.AutoSize = true;
            this.Tab2_LabelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.Tab2_LabelTitle.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Tab2_LabelTitle.Location = new System.Drawing.Point(24, 12);
            this.Tab2_LabelTitle.Name = "Tab2_LabelTitle";
            this.Tab2_LabelTitle.Size = new System.Drawing.Size(83, 36);
            this.Tab2_LabelTitle.TabIndex = 9;
            this.Tab2_LabelTitle.Text = "Download\r\nManager";
            this.Tab2_LabelTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Tab2_LabelTitle.Click += new System.EventHandler(this.Tab_Downloader_Click);
            // 
            // Tab2_LabelSubTitle
            // 
            this.Tab2_LabelSubTitle.AutoSize = true;
            this.Tab2_LabelSubTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.Tab2_LabelSubTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.Tab2_LabelSubTitle.Location = new System.Drawing.Point(16, 47);
            this.Tab2_LabelSubTitle.Name = "Tab2_LabelSubTitle";
            this.Tab2_LabelSubTitle.Size = new System.Drawing.Size(96, 15);
            this.Tab2_LabelSubTitle.TabIndex = 10;
            this.Tab2_LabelSubTitle.Text = "Scripts && Cheats";
            this.Tab2_LabelSubTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Tab2_LabelSubTitle.Click += new System.EventHandler(this.Tab_Downloader_Click);
            // 
            // Panel_Tab2Top
            // 
            this.Panel_Tab2Top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.Panel_Tab2Top.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Tab2Top.Location = new System.Drawing.Point(0, 0);
            this.Panel_Tab2Top.Name = "Panel_Tab2Top";
            this.Panel_Tab2Top.Size = new System.Drawing.Size(131, 10);
            this.Panel_Tab2Top.TabIndex = 7;
            this.Panel_Tab2Top.Click += new System.EventHandler(this.Tab_Downloader_Click);
            // 
            // Tab_Dashboard
            // 
            this.Tab_Dashboard.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Tab_Dashboard.BackColor = System.Drawing.Color.Transparent;
            this.Tab_Dashboard.Controls.Add(this.Tab1_LabelSubTitle);
            this.Tab_Dashboard.Controls.Add(this.Tab1_LabelTitle);
            this.Tab_Dashboard.Controls.Add(this.Panel_Tab1Top);
            this.Tab_Dashboard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Tab_Dashboard.Location = new System.Drawing.Point(191, 6);
            this.Tab_Dashboard.Name = "Tab_Dashboard";
            this.Tab_Dashboard.Size = new System.Drawing.Size(131, 66);
            this.Tab_Dashboard.TabIndex = 3;
            this.Tab_Dashboard.Click += new System.EventHandler(this.Tab_Dashboard_Click);
            // 
            // Tab1_LabelSubTitle
            // 
            this.Tab1_LabelSubTitle.AutoSize = true;
            this.Tab1_LabelSubTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.Tab1_LabelSubTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.Tab1_LabelSubTitle.Location = new System.Drawing.Point(19, 38);
            this.Tab1_LabelSubTitle.Name = "Tab1_LabelSubTitle";
            this.Tab1_LabelSubTitle.Size = new System.Drawing.Size(97, 15);
            this.Tab1_LabelSubTitle.TabIndex = 8;
            this.Tab1_LabelSubTitle.Text = "Manage Profiles";
            this.Tab1_LabelSubTitle.Click += new System.EventHandler(this.Tab_Dashboard_Click);
            // 
            // Tab1_LabelTitle
            // 
            this.Tab1_LabelTitle.AutoSize = true;
            this.Tab1_LabelTitle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Tab1_LabelTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.Tab1_LabelTitle.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Tab1_LabelTitle.Location = new System.Drawing.Point(21, 18);
            this.Tab1_LabelTitle.Name = "Tab1_LabelTitle";
            this.Tab1_LabelTitle.Size = new System.Drawing.Size(90, 18);
            this.Tab1_LabelTitle.TabIndex = 7;
            this.Tab1_LabelTitle.Text = "Dashboard";
            this.Tab1_LabelTitle.Click += new System.EventHandler(this.Tab_Dashboard_Click);
            // 
            // Panel_Tab1Top
            // 
            this.Panel_Tab1Top.BackColor = System.Drawing.Color.DodgerBlue;
            this.Panel_Tab1Top.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel_Tab1Top.Location = new System.Drawing.Point(0, 0);
            this.Panel_Tab1Top.Name = "Panel_Tab1Top";
            this.Panel_Tab1Top.Size = new System.Drawing.Size(131, 10);
            this.Panel_Tab1Top.TabIndex = 6;
            this.Panel_Tab1Top.Click += new System.EventHandler(this.Tab_Dashboard_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Impact", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.label2.Location = new System.Drawing.Point(62, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 45);
            this.label2.TabIndex = 14;
            this.label2.Text = "SHARP";
            this.label2.DoubleClick += new System.EventHandler(this.TopBar_DoubleClick);
            this.label2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Impact", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 45);
            this.label1.TabIndex = 13;
            this.label1.Text = "ELO";
            this.label1.DoubleClick += new System.EventHandler(this.TopBar_DoubleClick);
            this.label1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
            // 
            // panel_bottom_black_line
            // 
            this.panel_bottom_black_line.BackColor = System.Drawing.Color.Black;
            this.panel_bottom_black_line.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_bottom_black_line.Location = new System.Drawing.Point(0, 452);
            this.panel_bottom_black_line.Name = "panel_bottom_black_line";
            this.panel_bottom_black_line.Size = new System.Drawing.Size(796, 1);
            this.panel_bottom_black_line.TabIndex = 12;
            // 
            // statusStrip1
            // 
            this.statusStrip1.BackgroundImage = global::EloSharp.Properties.Resources.branding_bg;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.StatusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 453);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(796, 26);
            this.statusStrip1.SizingGrip = false;
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.toolStripStatusLabel1.ForeColor = System.Drawing.Color.White;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(56, 21);
            this.toolStripStatusLabel1.Text = "Status:";
            // 
            // StatusLabel
            // 
            this.StatusLabel.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.StatusLabel.ForeColor = System.Drawing.Color.Red;
            this.StatusLabel.Name = "StatusLabel";
            this.StatusLabel.Size = new System.Drawing.Size(90, 21);
            this.StatusLabel.Text = "Initializing...";
            // 
            // downloadManager
            // 
            this.downloadManager.Location = new System.Drawing.Point(77, 87);
            this.downloadManager.Name = "downloadManager";
            this.downloadManager.Size = new System.Drawing.Size(65, 54);
            this.downloadManager.TabIndex = 17;
            // 
            // Dashboard
            // 
            this.Dashboard.Location = new System.Drawing.Point(8, 87);
            this.Dashboard.Name = "Dashboard";
            this.Dashboard.Size = new System.Drawing.Size(63, 54);
            this.Dashboard.TabIndex = 16;
            // 
            // developerTools
            // 
            this.developerTools.Location = new System.Drawing.Point(8, 147);
            this.developerTools.Name = "developerTools";
            this.developerTools.Size = new System.Drawing.Size(63, 54);
            this.developerTools.TabIndex = 15;
            // 
            // ConsoleLog
            // 
            this.ConsoleLog.Location = new System.Drawing.Point(77, 147);
            this.ConsoleLog.Name = "ConsoleLog";
            this.ConsoleLog.Size = new System.Drawing.Size(65, 54);
            this.ConsoleLog.TabIndex = 14;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 479);
            this.Controls.Add(this.downloadManager);
            this.Controls.Add(this.Dashboard);
            this.Controls.Add(this.developerTools);
            this.Controls.Add(this.ConsoleLog);
            this.Controls.Add(this.panel_bottom_black_line);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.TopBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(780, 400);
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.MainForm_MouseDown);
            this.TopBar.ResumeLayout(false);
            this.TopBar.PerformLayout();
            this.Tab_Log.ResumeLayout(false);
            this.Tab_Log.PerformLayout();
            this.Tab_DeveloperTools.ResumeLayout(false);
            this.Tab_DeveloperTools.PerformLayout();
            this.Tab_Downloader.ResumeLayout(false);
            this.Tab_Downloader.PerformLayout();
            this.Tab_Dashboard.ResumeLayout(false);
            this.Tab_Dashboard.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel TopBar;
        private FlatTheme.FlatClose CloseForm;
        private FlatTheme.FlatMini MinimizeForm;
        private System.Windows.Forms.Panel Tab_Log;
        private System.Windows.Forms.Panel Panel_Tab4Top;
        private System.Windows.Forms.Label Tab4_LabelSubTitle;
        private System.Windows.Forms.Label Tab4_LabelTitle;
        private System.Windows.Forms.Panel Tab_DeveloperTools;
        private System.Windows.Forms.Panel Panel_Tab3Top;
        private System.Windows.Forms.Label Tab3_LabelSubTitle;
        private System.Windows.Forms.Label Tab3_LabelTitle;
        private System.Windows.Forms.Panel Tab_Downloader;
        private System.Windows.Forms.Label Tab2_LabelSubTitle;
        private System.Windows.Forms.Label Tab2_LabelTitle;
        private System.Windows.Forms.Panel Panel_Tab2Top;
        private System.Windows.Forms.Panel Tab_Dashboard;
        private System.Windows.Forms.Label Tab1_LabelSubTitle;
        private System.Windows.Forms.Label Tab1_LabelTitle;
        private System.Windows.Forms.Panel Panel_Tab1Top;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        public System.Windows.Forms.ToolStripStatusLabel StatusLabel;
        private System.Windows.Forms.Panel panel_bottom_black_line;
        public ConsoleLog ConsoleLog;
        public Dashboard Dashboard;
        public DeveloperTools developerTools;
        public DownloadManager downloadManager;
        private System.Windows.Forms.Label label3;
    }
}

