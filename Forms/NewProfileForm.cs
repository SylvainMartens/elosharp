﻿using System;
using System.Drawing;
using System.Windows.Forms;
using EloSharp.Managers;
namespace EloSharp.Forms
{
    public partial class NewProfileForm : Form
    {
        public NewProfileForm()
        {
            InitializeComponent();
        }
        private void Btn_Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void Txt_ProfileName_Enter(object sender, EventArgs e)
        {
            Txt_ProfileName.BaseColor = Color.FromArgb(45, 47, 49);
            if (Txt_ProfileName.Text == @"NAME") Txt_ProfileName.Text = "";
        }
        private void Txt_ProfileName_Leave(object sender, EventArgs e)
        {
            if (Txt_ProfileName.Text.Length == 0) Txt_ProfileName.Text = @"NAME";
        }
        private void Btn_CreateProfile_Click(object sender, EventArgs e)
        {
            if (Txt_ProfileName.Text.Length < 3) { Txt_ProfileName.BackColor = Color.Red; return; }
            ProfileManager.Create(Txt_ProfileName.Text);
            Close();
        }
    }
}
