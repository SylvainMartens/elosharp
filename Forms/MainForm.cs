﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using EloSharp.Managers;
namespace EloSharp.Forms
{
    public partial class MainForm : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        public int MainTabIndex = 0;
        private Thread InitThread { get; set; }
        /*
         * 0 = Dashboard
         * 1 = Downloader
         * 2 = Developper Tools
         * 3 = Log
         */
        public MainForm()
        {
            InitializeComponent();
            SwitchMainTab();
        }
        new private void Close()
        {
            Program.Ready = false;
            if (MemoryManager.MemoryThread != null)
                MemoryManager.MemoryThread.Abort();
            if (ObjectManager.WorkerThread != null)
                ObjectManager.WorkerThread.Abort();
            if (InitThread != null)
                InitThread.Abort();

            foreach (var script in ScriptManager.LoadedScripts.Where(o => o.Activated))
            {
                ScriptManager.Deactivate(script.Hash);
            }
            Application.Exit();
        }
        private void CloseForm_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void Tab_Dashboard_Click(object sender, EventArgs e)
        {
            MainTabIndex = 0;
            SwitchMainTab();
        }
        private void Tab_Downloader_Click(object sender, EventArgs e)
        {
            MainTabIndex = 1;
            SwitchMainTab();
        }
        private void Tab_DeveloperTools_Click(object sender, EventArgs e)
        {
            MainTabIndex = 2;
            SwitchMainTab();
        }
        private void Tab_Log_Click(object sender, EventArgs e)
        {
            MainTabIndex = 3;
            SwitchMainTab();
        }
        private void SwitchMainTab()
        {
            switch (MainTabIndex)
            {
                default:
                    Panel_Tab1Top.BackColor = Color.DodgerBlue;
                    Panel_Tab2Top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Panel_Tab3Top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Panel_Tab4Top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Tab1_LabelTitle.ForeColor = Color.DodgerBlue;
                    Tab2_LabelTitle.ForeColor = Color.Black;
                    Tab3_LabelTitle.ForeColor = Color.Black;
                    Tab4_LabelTitle.ForeColor = Color.Black;
                    
                    downloadManager.Hide();
                    downloadManager.Dock = DockStyle.None;
                    developerTools.Hide();
                    developerTools.Dock = DockStyle.None;
                    ConsoleLog.Hide();
                    ConsoleLog.Dock = DockStyle.None;

                    Dashboard.Dock = DockStyle.Fill;
                    Dashboard.Show();
                break;
                case 1:
                    Panel_Tab1Top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Panel_Tab2Top.BackColor = Color.DodgerBlue;
                    Panel_Tab3Top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Panel_Tab4Top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Tab1_LabelTitle.ForeColor = Color.Black;
                    Tab2_LabelTitle.ForeColor = Color.DodgerBlue;
                    Tab3_LabelTitle.ForeColor = Color.Black;
                    Tab4_LabelTitle.ForeColor = Color.Black;
                    Dashboard.Hide();
                    Dashboard.Dock = DockStyle.None;
                    developerTools.Hide();
                    developerTools.Dock = DockStyle.None;

                    downloadManager.Dock = DockStyle.Fill;
                    downloadManager.Show();
                break;
                case 2:
                    Panel_Tab1Top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Panel_Tab2Top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Panel_Tab3Top.BackColor = Color.DodgerBlue;
                    Panel_Tab4Top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Tab1_LabelTitle.ForeColor = Color.Black;
                    Tab2_LabelTitle.ForeColor = Color.Black;
                    Tab3_LabelTitle.ForeColor = Color.DodgerBlue;
                    Tab4_LabelTitle.ForeColor = Color.Black;
                    Dashboard.Hide();
                    Dashboard.Dock = DockStyle.None;
                    downloadManager.Hide();
                    downloadManager.Dock = DockStyle.None;
                    ConsoleLog.Hide();
                    ConsoleLog.Dock = DockStyle.None;

                    developerTools.Dock = DockStyle.Fill;
                    developerTools.Show();
                break;
                case 3:
                    Panel_Tab1Top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Panel_Tab2Top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Panel_Tab3Top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Panel_Tab4Top.BackColor = Color.DodgerBlue;
                    Tab1_LabelTitle.ForeColor = Color.Black;
                    Tab2_LabelTitle.ForeColor = Color.Black;
                    Tab3_LabelTitle.ForeColor = Color.Black;
                    Tab4_LabelTitle.ForeColor = Color.DodgerBlue;
                    Dashboard.Hide();
                    Dashboard.Dock = DockStyle.None;
                    downloadManager.Hide();
                    downloadManager.Dock = DockStyle.None;
                    developerTools.Hide();
                    developerTools.Dock = DockStyle.None;

                    ConsoleLog.Dock = DockStyle.Fill;
                    ConsoleLog.Show();
                break;
            }
        }
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!Program.Ready) return;
            e.Cancel = true;
            Close();
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            Directory.CreateDirectory(string.Format("{0}/EloSharp/", Environment.GetEnvironmentVariable("AppData")));
            File.Copy(Application.ExecutablePath, string.Format("{0}/EloSharp/EloSharp.elosharp", Environment.GetEnvironmentVariable("AppData")), true);

            if (Process.GetProcessesByName("Elo Sharp").Length > 1)
            {
                MessageBox.Show(@"Elo Sharp is already open!", @"Elo Sharp");
                Application.Exit();
                return;
            }
            if (!Functions.IsAdmin)
            {
                MessageBox.Show(@"You have to run Elo Sharp as administrator.", @"Elo Sharp");
                Application.Exit();
                return;
            }
            if (SettingsManager.ReadProperty("LOLDIRECTORY").Length == 0)
            {
                while (SettingsManager.ReadProperty("LOLDIRECTORY").Length <= 0)
                {
                    MessageBox.Show(@"Please Select Your League of Legends folder.", @"Elo Sharp");
                    var ofd = new FolderBrowserDialog();
                    if (ofd.ShowDialog() != DialogResult.OK) continue;
                    if (File.Exists(ofd.SelectedPath + "\\lol.launcher.exe"))
                    {
                        SettingsManager.WriteProperty("LOLDIRECTORY", ofd.SelectedPath);
                    }
                }
            }
            Text = @"Elo Sharp - " + Utils.Utils.Rand.Next(1) + @" V. " + Info.Build;
            Program.Ready = true;
            Debug.Log(string.Format("Elo Sharp Version {0} started...", Info.Build), MessageTypes.Info);
            InitThread = new Thread(() =>
            {
                try
                {
                    var info = new DirectoryInfo(string.Format("{0}/Scripts/", Application.StartupPath));
                    if (!info.Exists)
                    {
                        info.Create();
                    }
                    else
                    {
                        foreach (FileInfo file in info.GetFiles("*.dll", SearchOption.AllDirectories)) { file.Delete(); }
                        foreach (FileInfo file in info.GetFiles("*.pdb", SearchOption.AllDirectories)) { file.Delete(); }
                    }
                    MemoryManager.Initialize();
                    ScriptManager.Compile();
                    Debug.Log("Ready...", MessageTypes.Info);
                }
                catch (Exception ex)
                {
                    Debug.Log(ex.Message);
                }
            });
            InitThread.Start();
            ProfileManager.Initialize();
            if (Dashboard.Cmb_Profiles.Items.Count > 0 && Dashboard.SelectedProfile == "")
                Dashboard.Cmb_Profiles.SelectedIndex = 0;
        }

        private void TopBar_DoubleClick(object sender, EventArgs e)
        {
            WindowState = WindowState == FormWindowState.Maximized ? FormWindowState.Normal : FormWindowState.Maximized;
        }

        private void MainForm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Clicks != 1) return;
            if (e.Button != MouseButtons.Left) return;
            ReleaseCapture();
            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }
    }
}
