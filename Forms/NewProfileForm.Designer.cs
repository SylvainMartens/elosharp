﻿namespace EloSharp.Forms
{
    partial class NewProfileForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.formSkin1 = new FlatTheme.FormSkin();
            this.Btn_Cancel = new FlatTheme.FlatButton();
            this.Btn_CreateProfile = new FlatTheme.FlatButton();
            this.Txt_ProfileName = new FlatTheme.FlatTextBox();
            this.formSkin1.SuspendLayout();
            this.SuspendLayout();
            // 
            // formSkin1
            // 
            this.formSkin1.BackColor = System.Drawing.Color.White;
            this.formSkin1.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.formSkin1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(58)))), ((int)(((byte)(60)))));
            this.formSkin1.Controls.Add(this.Btn_Cancel);
            this.formSkin1.Controls.Add(this.Btn_CreateProfile);
            this.formSkin1.Controls.Add(this.Txt_ProfileName);
            this.formSkin1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.formSkin1.FlatColor = System.Drawing.Color.DodgerBlue;
            this.formSkin1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.formSkin1.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.formSkin1.HeaderLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(171)))), ((int)(((byte)(172)))));
            this.formSkin1.HeaderMaximize = false;
            this.formSkin1.Location = new System.Drawing.Point(0, 0);
            this.formSkin1.Movable = false;
            this.formSkin1.Name = "formSkin1";
            this.formSkin1.Size = new System.Drawing.Size(282, 136);
            this.formSkin1.TabIndex = 1;
            this.formSkin1.Text = "EloSharp // Add New Profile";
            this.formSkin1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            // 
            // Btn_Cancel
            // 
            this.Btn_Cancel.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Cancel.BaseColor = System.Drawing.Color.Red;
            this.Btn_Cancel.ButtonImage = null;
            this.Btn_Cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Cancel.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Btn_Cancel.ImageHeight = 0;
            this.Btn_Cancel.ImageLeft = 0;
            this.Btn_Cancel.ImageTop = 0;
            this.Btn_Cancel.ImageWidth = 0;
            this.Btn_Cancel.Location = new System.Drawing.Point(186, 92);
            this.Btn_Cancel.Name = "Btn_Cancel";
            this.Btn_Cancel.OnClickColor = System.Drawing.Color.Red;
            this.Btn_Cancel.OnMouseHover = System.Drawing.Color.Red;
            this.Btn_Cancel.OnMouseUpColor = System.Drawing.Color.Red;
            this.Btn_Cancel.Rounded = false;
            this.Btn_Cancel.Size = new System.Drawing.Size(86, 32);
            this.Btn_Cancel.TabIndex = 2;
            this.Btn_Cancel.Text = "Cancel";
            this.Btn_Cancel.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.Btn_Cancel.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.Btn_Cancel.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            this.Btn_Cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // Btn_CreateProfile
            // 
            this.Btn_CreateProfile.BackColor = System.Drawing.Color.Transparent;
            this.Btn_CreateProfile.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.Btn_CreateProfile.ButtonImage = null;
            this.Btn_CreateProfile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_CreateProfile.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Btn_CreateProfile.ImageHeight = 0;
            this.Btn_CreateProfile.ImageLeft = 0;
            this.Btn_CreateProfile.ImageTop = 0;
            this.Btn_CreateProfile.ImageWidth = 0;
            this.Btn_CreateProfile.Location = new System.Drawing.Point(12, 92);
            this.Btn_CreateProfile.Name = "Btn_CreateProfile";
            this.Btn_CreateProfile.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(57)))), ((int)(((byte)(51)))));
            this.Btn_CreateProfile.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.Btn_CreateProfile.OnMouseUpColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(57)))), ((int)(((byte)(51)))));
            this.Btn_CreateProfile.Rounded = false;
            this.Btn_CreateProfile.Size = new System.Drawing.Size(106, 32);
            this.Btn_CreateProfile.TabIndex = 1;
            this.Btn_CreateProfile.Text = "Create";
            this.Btn_CreateProfile.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.Btn_CreateProfile.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.Btn_CreateProfile.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            this.Btn_CreateProfile.Click += new System.EventHandler(this.Btn_CreateProfile_Click);
            // 
            // Txt_ProfileName
            // 
            this.Txt_ProfileName.BackColor = System.Drawing.Color.Transparent;
            this.Txt_ProfileName.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(49)))));
            this.Txt_ProfileName.Location = new System.Drawing.Point(12, 57);
            this.Txt_ProfileName.MaxLength = 32767;
            this.Txt_ProfileName.Multiline = false;
            this.Txt_ProfileName.Name = "Txt_ProfileName";
            this.Txt_ProfileName.ReadOnly = false;
            this.Txt_ProfileName.Size = new System.Drawing.Size(260, 29);
            this.Txt_ProfileName.TabIndex = 0;
            this.Txt_ProfileName.Text = "NAME";
            this.Txt_ProfileName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Txt_ProfileName.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Txt_ProfileName.UseSystemPasswordChar = false;
            this.Txt_ProfileName.Enter += new System.EventHandler(this.Txt_ProfileName_Enter);
            this.Txt_ProfileName.Leave += new System.EventHandler(this.Txt_ProfileName_Leave);
            // 
            // NewProfileForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 136);
            this.Controls.Add(this.formSkin1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "NewProfileForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "NewProfileForm";
            this.TransparencyKey = System.Drawing.Color.Fuchsia;
            this.formSkin1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private FlatTheme.FormSkin formSkin1;
        private FlatTheme.FlatButton Btn_Cancel;
        private FlatTheme.FlatButton Btn_CreateProfile;
        private FlatTheme.FlatTextBox Txt_ProfileName;
    }
}