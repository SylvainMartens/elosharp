﻿namespace EloSharp.UserControls
{
    partial class Dashboard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel6 = new System.Windows.Forms.Panel();
            this.Tab_skins_btn = new System.Windows.Forms.Panel();
            this.Tab_skins_label = new System.Windows.Forms.Label();
            this.Tab_skins_top = new System.Windows.Forms.Panel();
            this.Tab_cheats_btn = new System.Windows.Forms.Panel();
            this.Tab_cheats_label = new System.Windows.Forms.Label();
            this.Tab_cheats_top = new System.Windows.Forms.Panel();
            this.Tab_scripts_btn = new System.Windows.Forms.Panel();
            this.Tab_scripts_label = new System.Windows.Forms.Label();
            this.Tab_scripts_top = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flatButton1 = new FlatTheme.FlatButton();
            this.Btn_AddProfile = new FlatTheme.FlatButton();
            this.Btn_TrashProfile = new FlatTheme.FlatButton();
            this.Cmb_Profiles = new FlatTheme.FlatComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.DashboardScriptsTab = new EloSharp.UserControls.DashboardScriptsTab();
            this.panel6.SuspendLayout();
            this.Tab_skins_btn.SuspendLayout();
            this.Tab_cheats_btn.SuspendLayout();
            this.Tab_scripts_btn.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.Tab_skins_btn);
            this.panel6.Controls.Add(this.Tab_cheats_btn);
            this.panel6.Controls.Add(this.Tab_scripts_btn);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 43);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(798, 48);
            this.panel6.TabIndex = 13;
            // 
            // Tab_skins_btn
            // 
            this.Tab_skins_btn.BackColor = System.Drawing.Color.Transparent;
            this.Tab_skins_btn.Controls.Add(this.Tab_skins_label);
            this.Tab_skins_btn.Controls.Add(this.Tab_skins_top);
            this.Tab_skins_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Tab_skins_btn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tab_skins_btn.Location = new System.Drawing.Point(245, 0);
            this.Tab_skins_btn.Name = "Tab_skins_btn";
            this.Tab_skins_btn.Size = new System.Drawing.Size(308, 48);
            this.Tab_skins_btn.TabIndex = 8;
            this.Tab_skins_btn.Click += new System.EventHandler(this.Tab_skins_btn_Click);
            // 
            // Tab_skins_label
            // 
            this.Tab_skins_label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Tab_skins_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.Tab_skins_label.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Tab_skins_label.Location = new System.Drawing.Point(0, 10);
            this.Tab_skins_label.Name = "Tab_skins_label";
            this.Tab_skins_label.Size = new System.Drawing.Size(308, 38);
            this.Tab_skins_label.TabIndex = 9;
            this.Tab_skins_label.Text = "Skins";
            this.Tab_skins_label.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Tab_skins_label.Click += new System.EventHandler(this.Tab_skins_btn_Click);
            // 
            // Tab_skins_top
            // 
            this.Tab_skins_top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.Tab_skins_top.Dock = System.Windows.Forms.DockStyle.Top;
            this.Tab_skins_top.Location = new System.Drawing.Point(0, 0);
            this.Tab_skins_top.Name = "Tab_skins_top";
            this.Tab_skins_top.Size = new System.Drawing.Size(308, 10);
            this.Tab_skins_top.TabIndex = 7;
            // 
            // Tab_cheats_btn
            // 
            this.Tab_cheats_btn.BackColor = System.Drawing.Color.Transparent;
            this.Tab_cheats_btn.Controls.Add(this.Tab_cheats_label);
            this.Tab_cheats_btn.Controls.Add(this.Tab_cheats_top);
            this.Tab_cheats_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Tab_cheats_btn.Dock = System.Windows.Forms.DockStyle.Right;
            this.Tab_cheats_btn.Location = new System.Drawing.Point(553, 0);
            this.Tab_cheats_btn.Name = "Tab_cheats_btn";
            this.Tab_cheats_btn.Size = new System.Drawing.Size(245, 48);
            this.Tab_cheats_btn.TabIndex = 7;
            this.Tab_cheats_btn.Click += new System.EventHandler(this.Tab_cheats_btn_Click);
            // 
            // Tab_cheats_label
            // 
            this.Tab_cheats_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Tab_cheats_label.AutoSize = true;
            this.Tab_cheats_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.Tab_cheats_label.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Tab_cheats_label.Location = new System.Drawing.Point(96, 16);
            this.Tab_cheats_label.Name = "Tab_cheats_label";
            this.Tab_cheats_label.Size = new System.Drawing.Size(61, 18);
            this.Tab_cheats_label.TabIndex = 9;
            this.Tab_cheats_label.Text = "Cheats";
            this.Tab_cheats_label.Click += new System.EventHandler(this.Tab_cheats_btn_Click);
            // 
            // Tab_cheats_top
            // 
            this.Tab_cheats_top.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.Tab_cheats_top.Dock = System.Windows.Forms.DockStyle.Top;
            this.Tab_cheats_top.Location = new System.Drawing.Point(0, 0);
            this.Tab_cheats_top.Name = "Tab_cheats_top";
            this.Tab_cheats_top.Size = new System.Drawing.Size(245, 10);
            this.Tab_cheats_top.TabIndex = 7;
            this.Tab_cheats_top.Click += new System.EventHandler(this.Tab_cheats_btn_Click);
            // 
            // Tab_scripts_btn
            // 
            this.Tab_scripts_btn.BackColor = System.Drawing.Color.Transparent;
            this.Tab_scripts_btn.Controls.Add(this.Tab_scripts_label);
            this.Tab_scripts_btn.Controls.Add(this.Tab_scripts_top);
            this.Tab_scripts_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Tab_scripts_btn.Dock = System.Windows.Forms.DockStyle.Left;
            this.Tab_scripts_btn.Location = new System.Drawing.Point(0, 0);
            this.Tab_scripts_btn.Name = "Tab_scripts_btn";
            this.Tab_scripts_btn.Size = new System.Drawing.Size(245, 48);
            this.Tab_scripts_btn.TabIndex = 5;
            this.Tab_scripts_btn.Click += new System.EventHandler(this.Tab_scripts_btn_Click);
            // 
            // Tab_scripts_label
            // 
            this.Tab_scripts_label.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Tab_scripts_label.AutoSize = true;
            this.Tab_scripts_label.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Tab_scripts_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold);
            this.Tab_scripts_label.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Tab_scripts_label.Location = new System.Drawing.Point(88, 17);
            this.Tab_scripts_label.Name = "Tab_scripts_label";
            this.Tab_scripts_label.Size = new System.Drawing.Size(61, 18);
            this.Tab_scripts_label.TabIndex = 7;
            this.Tab_scripts_label.Text = "Scripts";
            this.Tab_scripts_label.Click += new System.EventHandler(this.Tab_scripts_btn_Click);
            // 
            // Tab_scripts_top
            // 
            this.Tab_scripts_top.BackColor = System.Drawing.Color.DodgerBlue;
            this.Tab_scripts_top.Dock = System.Windows.Forms.DockStyle.Top;
            this.Tab_scripts_top.Location = new System.Drawing.Point(0, 0);
            this.Tab_scripts_top.Name = "Tab_scripts_top";
            this.Tab_scripts_top.Size = new System.Drawing.Size(245, 10);
            this.Tab_scripts_top.TabIndex = 6;
            this.Tab_scripts_top.Click += new System.EventHandler(this.Tab_scripts_btn_Click);
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = global::EloSharp.Properties.Resources.branding_bg;
            this.panel1.Controls.Add(this.flatButton1);
            this.panel1.Controls.Add(this.Btn_AddProfile);
            this.panel1.Controls.Add(this.Btn_TrashProfile);
            this.panel1.Controls.Add(this.Cmb_Profiles);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(798, 43);
            this.panel1.TabIndex = 12;
            // 
            // flatButton1
            // 
            this.flatButton1.BackColor = System.Drawing.Color.Transparent;
            this.flatButton1.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.flatButton1.ButtonImage = null;
            this.flatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flatButton1.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flatButton1.ImageHeight = 0;
            this.flatButton1.ImageLeft = 0;
            this.flatButton1.ImageTop = 0;
            this.flatButton1.ImageWidth = 0;
            this.flatButton1.Location = new System.Drawing.Point(422, 10);
            this.flatButton1.Name = "flatButton1";
            this.flatButton1.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.flatButton1.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.flatButton1.OnMouseUpColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.flatButton1.Rounded = false;
            this.flatButton1.Size = new System.Drawing.Size(106, 27);
            this.flatButton1.TabIndex = 74;
            this.flatButton1.Text = "Rename Profile";
            this.flatButton1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.flatButton1.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.flatButton1.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            // 
            // Btn_AddProfile
            // 
            this.Btn_AddProfile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_AddProfile.BackColor = System.Drawing.Color.Transparent;
            this.Btn_AddProfile.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Btn_AddProfile.ButtonImage = null;
            this.Btn_AddProfile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_AddProfile.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_AddProfile.ImageHeight = 0;
            this.Btn_AddProfile.ImageLeft = 0;
            this.Btn_AddProfile.ImageTop = 0;
            this.Btn_AddProfile.ImageWidth = 0;
            this.Btn_AddProfile.Location = new System.Drawing.Point(655, 9);
            this.Btn_AddProfile.Name = "Btn_AddProfile";
            this.Btn_AddProfile.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(154)))), ((int)(((byte)(197)))));
            this.Btn_AddProfile.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.Btn_AddProfile.OnMouseUpColor = System.Drawing.Color.DodgerBlue;
            this.Btn_AddProfile.Rounded = false;
            this.Btn_AddProfile.Size = new System.Drawing.Size(130, 27);
            this.Btn_AddProfile.TabIndex = 72;
            this.Btn_AddProfile.Text = "Add New Profile";
            this.Btn_AddProfile.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.Btn_AddProfile.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.Btn_AddProfile.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            this.Btn_AddProfile.Click += new System.EventHandler(this.Btn_AddProfile_Click);
            // 
            // Btn_TrashProfile
            // 
            this.Btn_TrashProfile.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_TrashProfile.BackColor = System.Drawing.Color.Transparent;
            this.Btn_TrashProfile.BaseColor = System.Drawing.Color.Transparent;
            this.Btn_TrashProfile.ButtonImage = global::EloSharp.Properties.Resources.trashbin;
            this.Btn_TrashProfile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_TrashProfile.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Btn_TrashProfile.ImageHeight = 23;
            this.Btn_TrashProfile.ImageLeft = 5;
            this.Btn_TrashProfile.ImageTop = 0;
            this.Btn_TrashProfile.ImageWidth = 20;
            this.Btn_TrashProfile.Location = new System.Drawing.Point(617, 9);
            this.Btn_TrashProfile.Name = "Btn_TrashProfile";
            this.Btn_TrashProfile.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.Btn_TrashProfile.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.Btn_TrashProfile.OnMouseUpColor = System.Drawing.Color.Transparent;
            this.Btn_TrashProfile.Rounded = false;
            this.Btn_TrashProfile.Size = new System.Drawing.Size(32, 27);
            this.Btn_TrashProfile.TabIndex = 73;
            this.Btn_TrashProfile.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.Btn_TrashProfile.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.Btn_TrashProfile.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            this.Btn_TrashProfile.Click += new System.EventHandler(this.Btn_TrashProfile_Click);
            // 
            // Cmb_Profiles
            // 
            this.Cmb_Profiles.BackColor = System.Drawing.Color.White;
            this.Cmb_Profiles.BaseColor = System.Drawing.Color.Silver;
            this.Cmb_Profiles.BGColor = System.Drawing.Color.White;
            this.Cmb_Profiles.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cmb_Profiles.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.Cmb_Profiles.DropdownColor = System.Drawing.Color.Black;
            this.Cmb_Profiles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Profiles.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Profiles.ForeColor = System.Drawing.Color.Black;
            this.Cmb_Profiles.FormattingEnabled = true;
            this.Cmb_Profiles.HoverColor = System.Drawing.Color.DodgerBlue;
            this.Cmb_Profiles.ItemHeight = 18;
            this.Cmb_Profiles.Location = new System.Drawing.Point(168, 10);
            this.Cmb_Profiles.Name = "Cmb_Profiles";
            this.Cmb_Profiles.SelectedItemColor = System.Drawing.Color.Black;
            this.Cmb_Profiles.SelectedTextColor = System.Drawing.Color.Black;
            this.Cmb_Profiles.Size = new System.Drawing.Size(248, 24);
            this.Cmb_Profiles.TabIndex = 71;
            this.Cmb_Profiles.SelectedIndexChanged += new System.EventHandler(this.Cmb_Profiles_SelectedIndexChanged);
            this.Cmb_Profiles.SizeChanged += new System.EventHandler(this.Cmb_Profiles_SizeChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(6, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(156, 25);
            this.label9.TabIndex = 5;
            this.label9.Text = "Current Profile:";
            // 
            // DashboardScriptsTab
            // 
            this.DashboardScriptsTab.Location = new System.Drawing.Point(4, 97);
            this.DashboardScriptsTab.Name = "DashboardScriptsTab";
            this.DashboardScriptsTab.Size = new System.Drawing.Size(158, 171);
            this.DashboardScriptsTab.TabIndex = 14;
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.DashboardScriptsTab);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel1);
            this.Name = "Dashboard";
            this.Size = new System.Drawing.Size(798, 495);
            this.panel6.ResumeLayout(false);
            this.Tab_skins_btn.ResumeLayout(false);
            this.Tab_cheats_btn.ResumeLayout(false);
            this.Tab_cheats_btn.PerformLayout();
            this.Tab_scripts_btn.ResumeLayout(false);
            this.Tab_scripts_btn.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private FlatTheme.FlatButton flatButton1;
        private FlatTheme.FlatButton Btn_AddProfile;
        private FlatTheme.FlatButton Btn_TrashProfile;
        internal FlatTheme.FlatComboBox Cmb_Profiles;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel Tab_cheats_btn;
        private System.Windows.Forms.Label Tab_cheats_label;
        private System.Windows.Forms.Panel Tab_cheats_top;
        private System.Windows.Forms.Panel Tab_scripts_btn;
        private System.Windows.Forms.Label Tab_scripts_label;
        private System.Windows.Forms.Panel Tab_scripts_top;
        public DashboardScriptsTab DashboardScriptsTab;
        private System.Windows.Forms.Panel Tab_skins_btn;
        private System.Windows.Forms.Label Tab_skins_label;
        private System.Windows.Forms.Panel Tab_skins_top;
    }
}
