﻿using FastColoredTextBoxNS;

namespace EloSharp.UserControls
{
    partial class ConsoleLog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LogBox = new FastColoredTextBoxNS.FastColoredTextBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.button_clear_log = new FlatTheme.FlatButton();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.LogBox)).BeginInit();
            this.panel12.SuspendLayout();
            this.SuspendLayout();
            // 
            // LogBox
            // 
            this.LogBox.AutoScrollMinSize = new System.Drawing.Size(27, 14);
            this.LogBox.BackBrush = null;
            this.LogBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(47)))), ((int)(((byte)(49)))));
            this.LogBox.CharHeight = 14;
            this.LogBox.CharWidth = 8;
            this.LogBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.LogBox.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.LogBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LogBox.Font = new System.Drawing.Font("Courier New", 9.75F);
            this.LogBox.IndentBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.LogBox.IsReplaceMode = false;
            this.LogBox.LineNumberColor = System.Drawing.Color.Lime;
            this.LogBox.Location = new System.Drawing.Point(0, 43);
            this.LogBox.Name = "LogBox";
            this.LogBox.Paddings = new System.Windows.Forms.Padding(0);
            this.LogBox.ReadOnly = true;
            this.LogBox.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
            this.LogBox.Size = new System.Drawing.Size(693, 378);
            this.LogBox.TabIndex = 6;
            this.LogBox.Zoom = 100;
            // 
            // panel12
            // 
            this.panel12.BackgroundImage = global::EloSharp.Properties.Resources.branding_bg;
            this.panel12.Controls.Add(this.button_clear_log);
            this.panel12.Controls.Add(this.label10);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(693, 43);
            this.panel12.TabIndex = 4;
            // 
            // button_clear_log
            // 
            this.button_clear_log.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button_clear_log.BackColor = System.Drawing.Color.Transparent;
            this.button_clear_log.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.button_clear_log.ButtonImage = null;
            this.button_clear_log.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_clear_log.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_clear_log.ImageHeight = 0;
            this.button_clear_log.ImageLeft = 0;
            this.button_clear_log.ImageTop = 0;
            this.button_clear_log.ImageWidth = 0;
            this.button_clear_log.Location = new System.Drawing.Point(580, 7);
            this.button_clear_log.Name = "button_clear_log";
            this.button_clear_log.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(52)))), ((int)(((byte)(52)))));
            this.button_clear_log.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.button_clear_log.OnMouseUpColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(65)))), ((int)(((byte)(65)))));
            this.button_clear_log.Rounded = false;
            this.button_clear_log.Size = new System.Drawing.Size(106, 27);
            this.button_clear_log.TabIndex = 75;
            this.button_clear_log.Text = "Clear Log";
            this.button_clear_log.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.button_clear_log.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.button_clear_log.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            this.button_clear_log.Click += new System.EventHandler(this.button_clear_log_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(4, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(160, 25);
            this.label10.TabIndex = 6;
            this.label10.Text = "Application Log";
            // 
            // ConsoleLog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.LogBox);
            this.Controls.Add(this.panel12);
            this.Name = "ConsoleLog";
            this.Size = new System.Drawing.Size(693, 421);
            ((System.ComponentModel.ISupportInitialize)(this.LogBox)).EndInit();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label10;
        private FlatTheme.FlatButton button_clear_log;
        public FastColoredTextBox LogBox;
    }
}
