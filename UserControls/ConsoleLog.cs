﻿using System.Windows.Forms;

namespace EloSharp.UserControls
{
    public partial class ConsoleLog : UserControl
    {
        public ConsoleLog()
        {
            InitializeComponent();
        }

        private void button_clear_log_Click(object sender, System.EventArgs e)
        {
            LogBox.Text = "";
        }
    }
}
