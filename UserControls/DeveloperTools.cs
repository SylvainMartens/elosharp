﻿using System;
using System.Windows.Forms;
using EloSharp.Managers;
namespace EloSharp.UserControls
{
    public partial class DeveloperTools : UserControl
    {
        public bool DebugEnabled = true;
        public DeveloperTools()
        {
            InitializeComponent();
            var row0 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row0.Cells[0].Value = "MemoryManager.GameState";
            row0.Cells[1].Value = "Game Closed";
            dataGridView1.Rows.Add(row0);

            var row1 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row1.Cells[0].Value = "ObjectManager.Me.Health";
            row1.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row1);

            var row2 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row2.Cells[0].Value = "ObjectManager.Me.MaxHealth";
            row2.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row2);

            var row3 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row3.Cells[0].Value = "ObjectManager.Me.Mana";
            row3.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row3);

            var row4 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row4.Cells[0].Value = "ObjectManager.Me.MaxMana";
            row4.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row4);

            var row5 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row5.Cells[0].Value = "ObjectManager.Me.Gold";
            row5.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row5);

            var row6 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row6.Cells[0].Value = "ObjectManager.Me.Armor";
            row6.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row6);

            var row7 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row7.Cells[0].Value = "ObjectManager.Me.MagicRessist";
            row7.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row7);

            var row8 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row8.Cells[0].Value = "ObjectManager.Me.AbilityPower";
            row8.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row8);

            var row9 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row9.Cells[0].Value = "ObjectManager.Me.BaseDamage (AD?)";
            row9.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row9);

            var row10 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row10.Cells[0].Value = "ObjectManager.Me.AttackSpeed";
            row10.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row10);

            var row11 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row11.Cells[0].Value = "ObjectManager.Me.LifeSteal";
            row11.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row11);

            var row12 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row12.Cells[0].Value = "ObjectManager.Me.Critical";
            row12.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row12);

            var row13 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row13.Cells[0].Value = "ObjectManager.Me.CooldownRedution";
            row13.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row13);

            var row14 = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row14.Cells[0].Value = "ObjectManager.Me.MovementSpeed";
            row14.Cells[1].Value = "0";
            dataGridView1.Rows.Add(row14);
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!DebugEnabled || !Program.Ready) return;
            if (MemoryManager.Loaded && MemoryManager.LoL != null && ObjectManager.Me != null)
            {
                var gamestate = "Game Closed";
                switch (MemoryManager.GameState)
                {
                    case GameState.Loading:
                        gamestate = "0 - Loading Screen";
                    break;
                    case GameState.ObjectsSpawning:
                        gamestate = "1 - Spawning Objects";
                    break;
                    case GameState.InGame:
                        gamestate = "2 - In Game";
                    break;
                    case GameState.GameExiting:
                        gamestate = "3 - Game Exiting";
                    break;
                }
                dataGridView1.Rows[0].Cells[1].Value = gamestate;
                dataGridView1.Rows[1].Cells[1].Value = ObjectManager.Me.Health;
                dataGridView1.Rows[2].Cells[1].Value = ObjectManager.Me.MaxHealth;
                dataGridView1.Rows[3].Cells[1].Value = ObjectManager.Me.Mana;
                dataGridView1.Rows[4].Cells[1].Value = ObjectManager.Me.MaxMana;
                dataGridView1.Rows[5].Cells[1].Value = ObjectManager.Me.Gold;
                dataGridView1.Rows[6].Cells[1].Value = ObjectManager.Me.Armor;
                dataGridView1.Rows[7].Cells[1].Value = ObjectManager.Me.MagicRessist;
                dataGridView1.Rows[8].Cells[1].Value = ObjectManager.Me.AbilityPower;
                dataGridView1.Rows[9].Cells[1].Value = ObjectManager.Me.BaseDamage;
                dataGridView1.Rows[10].Cells[1].Value = ObjectManager.Me.AttackSpeed;
                dataGridView1.Rows[11].Cells[1].Value = ObjectManager.Me.LifeSteal;
                dataGridView1.Rows[12].Cells[1].Value = ObjectManager.Me.Critical;
                dataGridView1.Rows[13].Cells[1].Value = ObjectManager.Me.CooldownRedution;
                dataGridView1.Rows[14].Cells[1].Value = ObjectManager.Me.MovementSpeed;
            }
            else
            {
                dataGridView1.Rows[0].Cells[1].Value = "Game Closed";
            }
        }
    }
}
