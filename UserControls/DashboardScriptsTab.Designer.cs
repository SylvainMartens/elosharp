﻿namespace EloSharp.UserControls
{
    partial class DashboardScriptsTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.Btn_AddScript = new FlatTheme.FlatButton();
            this.Btn_ScriptsAdded = new FlatTheme.FlatButton();
            this.Btn_Refresh = new FlatTheme.FlatButton();
            this.ScriptControl = new System.Windows.Forms.Panel();
            this.ProfileLoadingLabel = new FlatTheme.FlatLabel();
            this.CockpitContentPanel = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.ScriptControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = global::EloSharp.Properties.Resources.branding_bg;
            this.panel2.Controls.Add(this.Btn_AddScript);
            this.panel2.Controls.Add(this.Btn_ScriptsAdded);
            this.panel2.Controls.Add(this.Btn_Refresh);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 488);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(902, 43);
            this.panel2.TabIndex = 24;
            // 
            // Btn_AddScript
            // 
            this.Btn_AddScript.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_AddScript.BackColor = System.Drawing.Color.Transparent;
            this.Btn_AddScript.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(17)))));
            this.Btn_AddScript.ButtonImage = null;
            this.Btn_AddScript.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_AddScript.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_AddScript.ImageHeight = 0;
            this.Btn_AddScript.ImageLeft = 0;
            this.Btn_AddScript.ImageTop = 0;
            this.Btn_AddScript.ImageWidth = 0;
            this.Btn_AddScript.Location = new System.Drawing.Point(763, 8);
            this.Btn_AddScript.Name = "Btn_AddScript";
            this.Btn_AddScript.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.Btn_AddScript.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.Btn_AddScript.OnMouseUpColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(17)))));
            this.Btn_AddScript.Rounded = false;
            this.Btn_AddScript.Size = new System.Drawing.Size(130, 27);
            this.Btn_AddScript.TabIndex = 72;
            this.Btn_AddScript.Text = "Add New Script";
            this.Btn_AddScript.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.Btn_AddScript.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.Btn_AddScript.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            this.Btn_AddScript.Click += new System.EventHandler(this.Btn_AddScript_Click);
            // 
            // Btn_ScriptsAdded
            // 
            this.Btn_ScriptsAdded.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_ScriptsAdded.BackColor = System.Drawing.Color.Transparent;
            this.Btn_ScriptsAdded.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(17)))));
            this.Btn_ScriptsAdded.ButtonImage = global::EloSharp.Properties.Resources.btn_ok;
            this.Btn_ScriptsAdded.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_ScriptsAdded.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_ScriptsAdded.ImageHeight = 20;
            this.Btn_ScriptsAdded.ImageLeft = 6;
            this.Btn_ScriptsAdded.ImageTop = 2;
            this.Btn_ScriptsAdded.ImageWidth = 17;
            this.Btn_ScriptsAdded.Location = new System.Drawing.Point(725, 8);
            this.Btn_ScriptsAdded.Name = "Btn_ScriptsAdded";
            this.Btn_ScriptsAdded.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(17)))));
            this.Btn_ScriptsAdded.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(17)))));
            this.Btn_ScriptsAdded.OnMouseUpColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(17)))));
            this.Btn_ScriptsAdded.Rounded = false;
            this.Btn_ScriptsAdded.Size = new System.Drawing.Size(86, 27);
            this.Btn_ScriptsAdded.TabIndex = 77;
            this.Btn_ScriptsAdded.Text = "I\'m done!";
            this.Btn_ScriptsAdded.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.Btn_ScriptsAdded.TextLeftRightAlign = System.Drawing.StringAlignment.Far;
            this.Btn_ScriptsAdded.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            this.Btn_ScriptsAdded.Visible = false;
            this.Btn_ScriptsAdded.Click += new System.EventHandler(this.Btn_ScriptsAdded_Click);
            // 
            // Btn_Refresh
            // 
            this.Btn_Refresh.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_Refresh.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Refresh.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.Btn_Refresh.ButtonImage = null;
            this.Btn_Refresh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Refresh.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_Refresh.ImageHeight = 23;
            this.Btn_Refresh.ImageLeft = 0;
            this.Btn_Refresh.ImageTop = 0;
            this.Btn_Refresh.ImageWidth = 20;
            this.Btn_Refresh.Location = new System.Drawing.Point(817, 8);
            this.Btn_Refresh.Name = "Btn_Refresh";
            this.Btn_Refresh.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.Btn_Refresh.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.Btn_Refresh.OnMouseUpColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.Btn_Refresh.Rounded = false;
            this.Btn_Refresh.Size = new System.Drawing.Size(76, 27);
            this.Btn_Refresh.TabIndex = 76;
            this.Btn_Refresh.Text = "Refresh";
            this.Btn_Refresh.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.Btn_Refresh.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.Btn_Refresh.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            // 
            // ScriptControl
            // 
            this.ScriptControl.BackColor = System.Drawing.Color.White;
            this.ScriptControl.Controls.Add(this.ProfileLoadingLabel);
            this.ScriptControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ScriptControl.Location = new System.Drawing.Point(0, 0);
            this.ScriptControl.Name = "ScriptControl";
            this.ScriptControl.Size = new System.Drawing.Size(902, 488);
            this.ScriptControl.TabIndex = 25;
            // 
            // ProfileLoadingLabel
            // 
            this.ProfileLoadingLabel.BackColor = System.Drawing.Color.Transparent;
            this.ProfileLoadingLabel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProfileLoadingLabel.Font = new System.Drawing.Font("Segoe UI", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ProfileLoadingLabel.ForeColor = System.Drawing.Color.Silver;
            this.ProfileLoadingLabel.Location = new System.Drawing.Point(0, 0);
            this.ProfileLoadingLabel.Name = "ProfileLoadingLabel";
            this.ProfileLoadingLabel.Size = new System.Drawing.Size(902, 488);
            this.ProfileLoadingLabel.TabIndex = 23;
            this.ProfileLoadingLabel.Text = "Select a profile...";
            this.ProfileLoadingLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CockpitContentPanel
            // 
            this.CockpitContentPanel.BackColor = System.Drawing.Color.White;
            this.CockpitContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CockpitContentPanel.Location = new System.Drawing.Point(0, 0);
            this.CockpitContentPanel.Name = "CockpitContentPanel";
            this.CockpitContentPanel.Size = new System.Drawing.Size(902, 488);
            this.CockpitContentPanel.TabIndex = 26;
            // 
            // DashboardScriptsTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ScriptControl);
            this.Controls.Add(this.CockpitContentPanel);
            this.Controls.Add(this.panel2);
            this.Name = "DashboardScriptsTab";
            this.Size = new System.Drawing.Size(902, 531);
            this.panel2.ResumeLayout(false);
            this.ScriptControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private FlatTheme.FlatButton Btn_AddScript;
        private FlatTheme.FlatButton Btn_ScriptsAdded;
        private FlatTheme.FlatButton Btn_Refresh;
        internal System.Windows.Forms.Panel ScriptControl;
        internal FlatTheme.FlatLabel ProfileLoadingLabel;
        internal System.Windows.Forms.Panel CockpitContentPanel;
    }
}
