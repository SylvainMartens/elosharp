﻿using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using EloSharp.Forms;
using EloSharp.Managers;

namespace EloSharp.UserControls
{
    public partial class Dashboard : UserControl
    {
        public string SelectedProfile = "";
        public int MainTabIndex = 0;
        public Dashboard()
        {
            InitializeComponent();
            SwitchTab();
        }
        private void Btn_AddProfile_Click(object sender, System.EventArgs e)
        {
            new NewProfileForm().ShowDialog();
        }
        private void Btn_TrashProfile_Click(object sender, System.EventArgs e)
        {
            if (ProfileManager.GetActiveProfile(Cmb_Profiles.Text) == null) return;
            ProfileManager.Remove(ProfileManager.ProfileList.First(o => o.Name == Cmb_Profiles.Text));
            if (ProfileManager.GetActiveProfile(Cmb_Profiles.Text) != null)
                foreach (string scriptHash in ProfileManager.GetActiveProfile(Cmb_Profiles.Text).ConnectedScripts)
                {
                    ScriptManager.Deactivate(scriptHash);
                }
            if (Cmb_Profiles.Text.Length != 0) return;
            DashboardScriptsTab.ProfileLoadingLabel.Text = @"Select a profile...";
            DashboardScriptsTab.ProfileLoadingLabel.Visible = true;
        }
        private void SwitchTab()
        {
            switch (MainTabIndex)
            {
                default:
                    Tab_scripts_top.BackColor = Color.DodgerBlue;
                    Tab_skins_top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Tab_cheats_top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Tab_scripts_label.ForeColor = Color.DodgerBlue;
                    Tab_skins_label.ForeColor = Color.Black;
                    Tab_cheats_label.ForeColor = Color.Black;

                    DashboardScriptsTab.Dock = DockStyle.Fill;
                    DashboardScriptsTab.Show();
                break;
                case 1:
                    Tab_scripts_top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Tab_skins_top.BackColor = Color.DodgerBlue;
                    Tab_cheats_top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Tab_scripts_label.ForeColor = Color.Black;
                    Tab_skins_label.ForeColor = Color.DodgerBlue;
                    Tab_cheats_label.ForeColor = Color.Black;
                    DashboardScriptsTab.Hide();
                    DashboardScriptsTab.Dock = DockStyle.None;
                break;
                case 2:
                    Tab_scripts_top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Tab_skins_top.BackColor = Color.FromArgb(255, 120, 120, 120);
                    Tab_cheats_top.BackColor = Color.DodgerBlue;
                    Tab_scripts_label.ForeColor = Color.Black;
                    Tab_skins_label.ForeColor = Color.Black;
                    Tab_cheats_label.ForeColor = Color.DodgerBlue;
                    DashboardScriptsTab.Hide();
                    DashboardScriptsTab.Dock = DockStyle.None;
                break;
            }
        }
        private void Tab_scripts_btn_Click(object sender, System.EventArgs e)
        {
            MainTabIndex = 0;
            SwitchTab();
        }
        private void Tab_skins_btn_Click(object sender, System.EventArgs e)
        {
            MainTabIndex = 1;
            SwitchTab();
        }
        private void Tab_cheats_btn_Click(object sender, System.EventArgs e)
        {
            MainTabIndex = 2;
            SwitchTab();
        }
        private void Cmb_Profiles_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (ProfileManager.GetActiveProfile(Cmb_Profiles.Text) == null)
            {
                DashboardScriptsTab.Clear();
                DashboardScriptsTab.ProfileLoadingLabel.Text = @"Select a profile...";
                DashboardScriptsTab.ProfileLoadingLabel.Visible = true;
                return;
            }
            SelectedProfile = Cmb_Profiles.Text;
            if (ProfileManager.GetActiveProfile(Cmb_Profiles.Text).ConnectedScripts.Count == 0)
            {
                DashboardScriptsTab.Clear();
                DashboardScriptsTab.ProfileLoadingLabel.Text = @"Now, add scripts to that profile!";
                DashboardScriptsTab.ProfileLoadingLabel.Visible = true;
            }
            else
            {
                DashboardScriptsTab.ProfileLoadingLabel.Visible = false;
                DashboardScriptsTab.DisplayScripts();
            }
        }

        private void Cmb_Profiles_SizeChanged(object sender, System.EventArgs e)
        {
            if (Cmb_Profiles.Items.Count > 0 && SelectedProfile == "")
                Cmb_Profiles.SelectedIndex = 0;
        }
    }
}
