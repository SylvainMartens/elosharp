﻿namespace EloSharp.UserControls
{
    partial class DownloadManager
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel14 = new System.Windows.Forms.Panel();
            this.flatButton7 = new FlatTheme.FlatButton();
            this.flatButton2 = new FlatTheme.FlatButton();
            this.flatButton4 = new FlatTheme.FlatButton();
            this.flatButton3 = new FlatTheme.FlatButton();
            this.panel12 = new System.Windows.Forms.Panel();
            this.Cmb_Profiles = new FlatTheme.FlatComboBox();
            this.Btn_AddProfile = new FlatTheme.FlatButton();
            this.textBox1 = new FlatTheme.FlatTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.panel14.SuspendLayout();
            this.panel12.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel14
            // 
            this.panel14.BackgroundImage = global::EloSharp.Properties.Resources.branding_bg;
            this.panel14.Controls.Add(this.flatButton7);
            this.panel14.Controls.Add(this.flatButton2);
            this.panel14.Controls.Add(this.flatButton4);
            this.panel14.Controls.Add(this.flatButton3);
            this.panel14.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel14.Location = new System.Drawing.Point(0, 398);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(562, 43);
            this.panel14.TabIndex = 4;
            // 
            // flatButton7
            // 
            this.flatButton7.BackColor = System.Drawing.Color.Transparent;
            this.flatButton7.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.flatButton7.ButtonImage = null;
            this.flatButton7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flatButton7.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flatButton7.ImageHeight = 0;
            this.flatButton7.ImageLeft = 0;
            this.flatButton7.ImageTop = 0;
            this.flatButton7.ImageWidth = 0;
            this.flatButton7.Location = new System.Drawing.Point(139, 8);
            this.flatButton7.Name = "flatButton7";
            this.flatButton7.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.flatButton7.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.flatButton7.OnMouseUpColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.flatButton7.Rounded = false;
            this.flatButton7.Size = new System.Drawing.Size(100, 27);
            this.flatButton7.TabIndex = 79;
            this.flatButton7.Text = "Update";
            this.flatButton7.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.flatButton7.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.flatButton7.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            // 
            // flatButton2
            // 
            this.flatButton2.BackColor = System.Drawing.Color.Transparent;
            this.flatButton2.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.flatButton2.ButtonImage = null;
            this.flatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flatButton2.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flatButton2.ImageHeight = 0;
            this.flatButton2.ImageLeft = 0;
            this.flatButton2.ImageTop = 0;
            this.flatButton2.ImageWidth = 0;
            this.flatButton2.Location = new System.Drawing.Point(245, 8);
            this.flatButton2.Name = "flatButton2";
            this.flatButton2.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.flatButton2.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.flatButton2.OnMouseUpColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.flatButton2.Rounded = false;
            this.flatButton2.Size = new System.Drawing.Size(100, 27);
            this.flatButton2.TabIndex = 78;
            this.flatButton2.Text = "Update All";
            this.flatButton2.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.flatButton2.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.flatButton2.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            // 
            // flatButton4
            // 
            this.flatButton4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.flatButton4.BackColor = System.Drawing.Color.Transparent;
            this.flatButton4.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(46)))), ((int)(((byte)(46)))));
            this.flatButton4.ButtonImage = global::EloSharp.Properties.Resources.trashbin;
            this.flatButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flatButton4.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.flatButton4.ImageHeight = 23;
            this.flatButton4.ImageLeft = 5;
            this.flatButton4.ImageTop = 0;
            this.flatButton4.ImageWidth = 20;
            this.flatButton4.Location = new System.Drawing.Point(9, 8);
            this.flatButton4.Name = "flatButton4";
            this.flatButton4.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.flatButton4.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.flatButton4.OnMouseUpColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.flatButton4.Rounded = false;
            this.flatButton4.Size = new System.Drawing.Size(124, 27);
            this.flatButton4.TabIndex = 74;
            this.flatButton4.Text = "Uninstall";
            this.flatButton4.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.flatButton4.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.flatButton4.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            // 
            // flatButton3
            // 
            this.flatButton3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flatButton3.BackColor = System.Drawing.Color.Transparent;
            this.flatButton3.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(17)))));
            this.flatButton3.ButtonImage = null;
            this.flatButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.flatButton3.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flatButton3.ImageHeight = 0;
            this.flatButton3.ImageLeft = 0;
            this.flatButton3.ImageTop = 0;
            this.flatButton3.ImageWidth = 0;
            this.flatButton3.Location = new System.Drawing.Point(425, 8);
            this.flatButton3.Name = "flatButton3";
            this.flatButton3.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.flatButton3.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.flatButton3.OnMouseUpColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.flatButton3.Rounded = false;
            this.flatButton3.Size = new System.Drawing.Size(130, 27);
            this.flatButton3.TabIndex = 73;
            this.flatButton3.Text = "Download & Install";
            this.flatButton3.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.flatButton3.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.flatButton3.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            // 
            // panel12
            // 
            this.panel12.BackgroundImage = global::EloSharp.Properties.Resources.branding_bg;
            this.panel12.Controls.Add(this.Cmb_Profiles);
            this.panel12.Controls.Add(this.Btn_AddProfile);
            this.panel12.Controls.Add(this.textBox1);
            this.panel12.Controls.Add(this.label10);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(0, 0);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(562, 43);
            this.panel12.TabIndex = 5;
            // 
            // Cmb_Profiles
            // 
            this.Cmb_Profiles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Cmb_Profiles.BackColor = System.Drawing.Color.White;
            this.Cmb_Profiles.BaseColor = System.Drawing.Color.Silver;
            this.Cmb_Profiles.BGColor = System.Drawing.Color.White;
            this.Cmb_Profiles.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cmb_Profiles.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.Cmb_Profiles.DropdownColor = System.Drawing.Color.Black;
            this.Cmb_Profiles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Cmb_Profiles.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmb_Profiles.ForeColor = System.Drawing.Color.Black;
            this.Cmb_Profiles.FormattingEnabled = true;
            this.Cmb_Profiles.HoverColor = System.Drawing.Color.DodgerBlue;
            this.Cmb_Profiles.ItemHeight = 18;
            this.Cmb_Profiles.Items.AddRange(new object[] {
            "All",
            "Scripts",
            "Cheats",
            "Plugins",
            "Exploits"});
            this.Cmb_Profiles.Location = new System.Drawing.Point(184, 11);
            this.Cmb_Profiles.Name = "Cmb_Profiles";
            this.Cmb_Profiles.SelectedItemColor = System.Drawing.Color.Black;
            this.Cmb_Profiles.SelectedTextColor = System.Drawing.Color.Black;
            this.Cmb_Profiles.Size = new System.Drawing.Size(160, 24);
            this.Cmb_Profiles.TabIndex = 72;
            // 
            // Btn_AddProfile
            // 
            this.Btn_AddProfile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Btn_AddProfile.BackColor = System.Drawing.Color.Transparent;
            this.Btn_AddProfile.BaseColor = System.Drawing.Color.DodgerBlue;
            this.Btn_AddProfile.ButtonImage = null;
            this.Btn_AddProfile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_AddProfile.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btn_AddProfile.ImageHeight = 0;
            this.Btn_AddProfile.ImageLeft = 0;
            this.Btn_AddProfile.ImageTop = 0;
            this.Btn_AddProfile.ImageWidth = 0;
            this.Btn_AddProfile.Location = new System.Drawing.Point(482, 9);
            this.Btn_AddProfile.Name = "Btn_AddProfile";
            this.Btn_AddProfile.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(75)))), ((int)(((byte)(154)))), ((int)(((byte)(197)))));
            this.Btn_AddProfile.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.Btn_AddProfile.OnMouseUpColor = System.Drawing.Color.FromArgb(((int)(((byte)(83)))), ((int)(((byte)(164)))), ((int)(((byte)(207)))));
            this.Btn_AddProfile.Rounded = false;
            this.Btn_AddProfile.Size = new System.Drawing.Size(73, 29);
            this.Btn_AddProfile.TabIndex = 78;
            this.Btn_AddProfile.Text = "Search";
            this.Btn_AddProfile.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.Btn_AddProfile.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.Btn_AddProfile.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.BackColor = System.Drawing.Color.Transparent;
            this.textBox1.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.textBox1.Location = new System.Drawing.Point(347, 9);
            this.textBox1.Margin = new System.Windows.Forms.Padding(0);
            this.textBox1.MaxLength = 32767;
            this.textBox1.Multiline = false;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = false;
            this.textBox1.Size = new System.Drawing.Size(132, 29);
            this.textBox1.TabIndex = 79;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.textBox1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.textBox1.UseSystemPasswordChar = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(4, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(198, 25);
            this.label10.TabIndex = 6;
            this.label10.Text = "Download Manager";
            // 
            // DownloadManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel14);
            this.Name = "DownloadManager";
            this.Size = new System.Drawing.Size(562, 441);
            this.panel14.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label10;
        internal FlatTheme.FlatComboBox Cmb_Profiles;
        private FlatTheme.FlatButton flatButton3;
        private FlatTheme.FlatTextBox textBox1;
        private FlatTheme.FlatButton Btn_AddProfile;
        private FlatTheme.FlatButton flatButton4;
        private FlatTheme.FlatButton flatButton7;
        private FlatTheme.FlatButton flatButton2;
    }
}
