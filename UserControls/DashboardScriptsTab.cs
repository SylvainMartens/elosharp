﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using EloSharp.Managers;
using FlatTheme;

namespace EloSharp.UserControls
{
    public partial class DashboardScriptsTab : UserControl
    {
        private static readonly object DisplayScriptsLock = new object();
        internal static List<Panel> PanelList = new List<Panel>();

        public DashboardScriptsTab()
        {
            InitializeComponent();
        }

        private void Btn_AddScript_Click(object sender, System.EventArgs e)
        {
            if (ProfileManager.GetActiveProfile(Program.MainForm.Dashboard.Cmb_Profiles.Text) == null)
            {
                Clear();
                ProfileLoadingLabel.Text = @"Select a profile...";
                ProfileLoadingLabel.Visible = true;
                return;
            }
            DisplayScripts(true);
            Btn_AddScript.Visible = false;
            Btn_ScriptsAdded.Visible = true;
            Btn_Refresh.Visible = true;
        }
        internal void Clear()
        {
            Program.MainForm.Dashboard.Invoke((MethodInvoker)delegate()
            {
                foreach (Control cntrl in Program.MainForm.Dashboard.DashboardScriptsTab.ScriptControl.Controls)
                {
                    if (cntrl.Name == "ProfileLoadingLabel")
                    {
                        cntrl.Hide();
                        continue;
                    };
                    cntrl.Dispose();
                }
            });
        }
        internal void DisplayScripts(bool addToProfile = false)
        {
            new Thread(delegate()
            {
                while (ScriptManager.IsLoading) Thread.Sleep(10);
                lock (DisplayScriptsLock)
                {
                    var controlsToBeAdded = new List<Control>();
                    int i = 0; //Counts till 2
                    int x = 0; //Counts each row
                    int pt = 0; //Counts every time
                    int pages = Convert.ToInt32(Math.Ceiling((double)ScriptManager.LoadedScripts.Count / 9));
                    PanelList.Clear();
                    Clear();
                    for (int y = 0; y < pages; y++)
                    {
                        var tmp = new Panel { Dock = DockStyle.Fill, Visible = false };
                        PanelList.Add(tmp);
                        Program.MainForm.Dashboard.Invoke((MethodInvoker)(() =>
                            Program.MainForm.Dashboard.DashboardScriptsTab.ScriptControl.Controls.Add(PanelList[y])));
                    }
                    foreach (ScriptManager.Script scrpt in ScriptManager.LoadedScripts)
                    {
                        if (addToProfile) if (ProfileManager.GetActiveProfile(Program.MainForm.Dashboard.SelectedProfile).ConnectedScripts.Contains(scrpt.Hash)) break;
                        if (pt == 9)
                        {
                            x = 0;
                            i = 0;
                        }
                        int row = (x * 115);
                        var scriptLogo = new PictureBox { Location = new Point(25 + (i * 306), 13 + row), Size = new Size(71, 63), BackgroundImage = Properties.Resources.short_logo_dark, BackgroundImageLayout = ImageLayout.Stretch };
                        var scriptTitle = new Label { Location = new Point(113 + (i * 306), 13 + row), Text = scrpt.Name, Font = new Font("Segoe UI", 10), ForeColor = Color.FromArgb(65, 66, 66), Size = new Size(200, 15) };
                        var authorTitle = new Label { Location = new Point(113 + (i * 306), 35 + row), Text = string.Format("Author: {0}", scrpt.Author), Font = new Font("Segoe UI", 9), ForeColor = Color.Gray, Size = new Size(200, 15) };
                        var deleteButton = new FlatButton { Location = new Point(115 + (i * 306), 55 + row), Text = "", ButtonImage = Properties.Resources.trashbin, ImageWidth = 16, ImageHeight = 19, ImageLeft = 3, ImageTop = 3, Height = 26, Width = 24, OnClickColor = Color.FromArgb(33, 33, 33), OnMouseUpColor = Color.FromArgb(33, 33, 33), BaseColor = Color.FromArgb(33, 33, 33) };
                        var configureButton = new FlatButton { Location = new Point(138 + (i * 306), 55 + row), Text = "Configure", Width = 79, Font = new Font("Segoe UI", 10), Height = 26, OnClickColor = Color.FromArgb(33, 33, 33), OnMouseHover = Color.FromArgb(33, 33, 33), OnMouseUpColor = Color.FromArgb(33, 33, 33), BaseColor = Color.FromArgb(33, 33, 33) };
                        var disabledButton = new FlatButton { Location = new Point(220 + (i * 306), 55 + row), Text = "  Disabled", Width = 79, Font = new Font("Segoe UI", 10), Height = 26, OnClickColor = Color.FromArgb(209, 55, 5), OnMouseHover = Color.FromArgb(209, 55, 5), OnMouseUpColor = Color.FromArgb(209, 55, 5), BaseColor = Color.FromArgb(209, 55, 5), ButtonImage = Properties.Resources.btn_close, ImageWidth = 9, ImageHeight = 12, ImageLeft = 4, ImageTop = 6, };
                        var enabledButton = new FlatButton { Location = new Point(220 + (i * 306), 55 + row), Text = "    Enabled", Width = 79, Font = new Font("Segoe UI", 10), Height = 26, OnClickColor = Color.FromArgb(128, 168, 17), OnMouseHover = Color.FromArgb(128, 168, 17), OnMouseUpColor = Color.FromArgb(128, 168, 17), BaseColor = Color.FromArgb(128, 168, 17), ButtonImage = Properties.Resources.btn_ok, ImageWidth = 16, ImageHeight = 19, ImageLeft = 3, ImageTop = 3 };
                        var addToProfileButton = new FlatButton { Location = new Point(113 + (i * 306), 55 + row), Text = "    Add", Width = 79, Font = new Font("Segoe UI", 10), Height = 26, OnClickColor = Color.FromArgb(128, 168, 17), OnMouseHover = Color.FromArgb(128, 168, 17), OnMouseUpColor = Color.FromArgb(128, 168, 17), BaseColor = Color.FromArgb(128, 168, 17), ButtonImage = Properties.Resources.btn_ok, ImageWidth = 16, ImageHeight = 19, ImageLeft = 3, ImageTop = 3 };
                        deleteButton.Click += (e, sender) =>
                        {
                            ProfileManager.UninstallScript(scrpt);
                            //ScriptManager.Delete(scrpt);
                        };
                        configureButton.Click += (e, sender) => { scrpt.Interface.ShowForm(); };
                        disabledButton.Click += (e, sender) => { ScriptManager.Activate(scrpt.Hash); disabledButton.Hide(); enabledButton.Show(); };
                        enabledButton.Click += (e, sender) => { ScriptManager.Deactivate(scrpt.Hash); enabledButton.Hide(); disabledButton.Show(); };
                        controlsToBeAdded.Add(scriptLogo);
                        controlsToBeAdded.Add(scriptTitle);
                        controlsToBeAdded.Add(authorTitle);
                        if (!addToProfile)
                        {
                            controlsToBeAdded.Add(deleteButton);
                            controlsToBeAdded.Add(configureButton);
                            controlsToBeAdded.Add(enabledButton);
                            controlsToBeAdded.Add(disabledButton);
                            enabledButton.Hide();
                            disabledButton.Hide();
                            if (scrpt.Activated)
                            {
                                enabledButton.Show();
                            }
                            else
                            {
                                disabledButton.Show();
                            }
                        }
                        else
                        {
                            addToProfileButton.Click += (e, sender) => { ProfileManager.InstallScript(scrpt); addToProfileButton.Text = "Added!"; };
                            controlsToBeAdded.Add(addToProfileButton);
                        }
                        i++;
                        pt++;
                        if (i == 3) //Switch to next row.
                        {
                            x++;
                            i = 0;
                        }
                        Program.MainForm.Dashboard.Invoke((MethodInvoker)delegate()
                        {
                            PanelList[Convert.ToInt32(Math.Floor(((double)pt / 10)))].Controls.AddRange(controlsToBeAdded.ToArray());
                            controlsToBeAdded.Clear();
                        });
                    }
                    Program.MainForm.Dashboard.Invoke((MethodInvoker)delegate()
                    {
                        if (PanelList.Count > 0)
                        {
                            PanelList[0].Show();
                        }
                        else
                        {
                            Program.MainForm.Dashboard.DashboardScriptsTab.ProfileLoadingLabel.Visible = true;
                            Program.MainForm.Dashboard.DashboardScriptsTab.ProfileLoadingLabel.Text = @"No scripts to show :(";
                        }
                    });
                    if (pages <= 1) return;
                    for (int p = 0; p < pages; p++)
                    {
                        Image image = (p == 0) ? Properties.Resources.blue_rect : Properties.Resources.inact_rect;
                        var rect = new PictureBox
                        {
                            Size = new Size(11, 11),
                            BackgroundImage = image,
                            Location = new Point((p * 15) + 22, 355),
                            Visible = true,
                            Tag = p
                        };
                        rect.Click += (e, sender) =>
                        {
                            foreach (var cntrl in Program.MainForm.Dashboard.DashboardScriptsTab.CockpitContentPanel.Controls.OfType<PictureBox>().Where(cntrl => !cntrl.Name.Contains("Arrow")))
                            {
                                cntrl.BackgroundImage = Properties.Resources.inact_rect;
                            }
                            PanelList.ForEach(panel => panel.Hide());
                            PanelList[Convert.ToInt32(rect.Tag)].Show();
                            rect.BackgroundImage = Properties.Resources.blue_rect;
                        };
                        Program.MainForm.Dashboard.Invoke((MethodInvoker)(() =>
                            Program.MainForm.Dashboard.DashboardScriptsTab.CockpitContentPanel.Controls.Add(rect)));
                    }
                }
            }).Start();
        }
        private void Btn_ScriptsAdded_Click(object sender, EventArgs e)
        {
            if (ProfileManager.ActiveProfile.ConnectedScripts.Count == 0) //User didnt added any scripts
            {
                Clear();
                ProfileLoadingLabel.Text = @"Now, add scripts to that profile";
                ProfileLoadingLabel.Visible = true;
            }
            else
            {
                DisplayScripts();
            }
            Btn_ScriptsAdded.Visible = false;
            Btn_Refresh.Visible = false;
            Btn_AddScript.Visible = true;
        }
    }
}
