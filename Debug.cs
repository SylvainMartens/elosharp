﻿using System.Windows.Forms;
using FastColoredTextBoxNS;
using System;
using System.Drawing;
using System.IO;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
namespace EloSharp
{
    public class Logging
    {
        public static void Write(string message, Style style)
        {
        }
        public static void Write(string message)
        {
        }
    }
    public class Debug
    {
        public static void Log(string message, TextStyle style)
        {
            if (!Program.Ready) return;
            Program.MainForm.Invoke((MethodInvoker)delegate
            {
                if (style == MessageTypes.Error)
                {
                    message = string.Format("[{0}:{1}:{2} - Error]: {3}{4}",
                        DateTime.Now.Hour,
                        DateTime.Now.Minute,
                        DateTime.Now.Second,
                        message,
                        Environment.NewLine);
                }
                message = string.Format("[{0}:{1}:{2}]: {3}{4}",
                    DateTime.Now.Hour,
                    DateTime.Now.Minute,
                    DateTime.Now.Second,
                    message,
                    Environment.NewLine);

                message = string.Format("[{0}]: {1} {2}", DateTime.Now.ToString("HH:MM:ss"), message, Environment.NewLine);
                Program.MainForm.ConsoleLog.LogBox.BeginUpdate();
                Program.MainForm.ConsoleLog.LogBox.Update();
                Program.MainForm.ConsoleLog.LogBox.Selection.BeginUpdate();
                var userSelection = Program.MainForm.ConsoleLog.LogBox.Selection.Clone();
                Program.MainForm.ConsoleLog.LogBox.AppendText(message, style);
                if (!userSelection.IsEmpty || userSelection.Start.iLine < Program.MainForm.ConsoleLog.LogBox.LinesCount - 2)
                {
                    Program.MainForm.ConsoleLog.LogBox.Selection.Start = userSelection.Start;
                    Program.MainForm.ConsoleLog.LogBox.Selection.End = userSelection.End;
                }
                else
                    Program.MainForm.ConsoleLog.LogBox.GoEnd();

                Program.MainForm.ConsoleLog.LogBox.Selection.EndUpdate();
                Program.MainForm.ConsoleLog.LogBox.EndUpdate();
            });
        }

        public static void Log(string message)
        {
            Log(message, MessageTypes.Default);
        }

        internal static void Status(string message)
        {
            if (!Program.Ready) return;
            Program.MainForm.Invoke((MethodInvoker)delegate
            {
                Program.MainForm.StatusLabel.Text = message;
                Program.MainForm.StatusLabel.ForeColor = Color.White;
            });
        }

        internal static void Status(string message, Color fontColor)
        {
            if (!Program.Ready) return;
            Program.MainForm.Invoke((MethodInvoker)delegate
            {
                Program.MainForm.StatusLabel.Text = message;
                Program.MainForm.StatusLabel.ForeColor = fontColor;
            });
        }
    }

    internal class Functions
    {
        internal static IDisposable SetTimeout(Action method, int delayInMilliseconds)
        {
            var timer = new System.Timers.Timer(delayInMilliseconds);
            timer.Elapsed += (source, e) => method();
            timer.AutoReset = false;
            timer.Enabled = true;
            timer.Start();
            // Returns a stop handle which can be used for stopping
            // the timer, if required
            return timer;
        }
        internal static void DownloadExe(string output, string url)
        {
            //File.WriteAllBytes(output, WebReq.GetBytes(url));
        }
        internal static string GetMd5HashFromFile(string fileName)
        {
            var file = new FileStream(fileName, FileMode.Open);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(file);
            file.Close();
            var sb = new StringBuilder();
            foreach (byte t in retVal)
            {
                sb.Append(t.ToString("x2"));
            }
            return sb.ToString();
        }
        internal static bool IsAdmin
        {
            get
            {
                WindowsIdentity identity = WindowsIdentity.GetCurrent();
                if (identity == null) return false;
                var principal = new WindowsPrincipal(identity);
                return principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
        }
        internal static DateTime TimestampToDateTime(double unixTimeStamp)
        {
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }
        internal static int DateTimeToTimestamp(DateTime time)
        {
            var date1 = new DateTime(1970, 1, 1);
            var date2 = time;
            var ts = new TimeSpan(date2.Ticks - date1.Ticks);
            return (Convert.ToInt32(ts.TotalSeconds));
        }
    }
}