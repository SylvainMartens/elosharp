﻿using System;
using System.Drawing;
using System.Reflection;
using FastColoredTextBoxNS;
namespace EloSharp
{
    public struct Vector3
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
    }
    public class MessageTypes
    {
        public static TextStyle Default = new TextStyle(Brushes.White, null, FontStyle.Regular);
        public static TextStyle Info = new TextStyle(Brushes.DodgerBlue, null, FontStyle.Regular);
        public static TextStyle Warning = new TextStyle(Brushes.BurlyWood, null, FontStyle.Regular);
        public static TextStyle Error = new TextStyle(Brushes.Red, null, FontStyle.Regular);
        public static TextStyle Success = new TextStyle(Brushes.Green, null, FontStyle.Regular);
        public static TextStyle Critical = new TextStyle(Brushes.DarkRed, null, FontStyle.Regular);
    }
    public enum SpellState
    {
        Ready = 0,
        Used = 4,
        NotLearned = 2,
        NoResource = 5,
    }
    public enum ItemState
    {
        Ready = 0,
        Used = 4,
    }
    public class StringValue : Attribute
    {
        private readonly string _value;

        public StringValue(string value)
        {
            _value = value;
        }

        public string Value
        {
            get { return _value; }
        }

    }

    public static class StringEnum
    {
        public static string GetStringValue(Enum value)
        {
            string output = null;
            Type type = value.GetType();
            FieldInfo fi = type.GetField(value.ToString());
            var attrs = fi.GetCustomAttributes(typeof(StringValue), false) as StringValue[];
            if (attrs != null && attrs.Length > 0)
            {
                output = attrs[0].Value;
            }
            return output;
        }
    }

    public enum EmoteType
    {
        [StringValue("/dance")]
        Dance = 1,
        [StringValue("/taunt")]
        Taunt = 2,
        [StringValue("/laugh")]
        Laugh = 3,
        [StringValue("/joke")]
        Joke = 4,
    }

    public enum MoveType
    {
        Move = 2,
        Attack = 3,
        MoveStop = 0xA
    }

    public enum ObjectType
    {
        Player = 10,
        Minion = 21,
        SightWard = 9,
        VisionWard = 1001,
        Creeps = 11,
        Turret = 14,
        TurretCannon = 16, //The Turret_*_A one
        SpawnBarracks = 29,
        Laser = 24,
        LaserCannon = 26, //The ChaosTurret_*_A one
        Inhibitor = 1000,
        Unknown = 0
    }
    public class SummonerSpells
    {
        public static string Ghost { get { return "SummonerHaste"; } }
        public static string Smite { get { return "SummonerSmite"; } }
        public static string Revive { get { return "SummonerRevive"; } }
        public static string Clarity { get { return "SummonerMana"; } }
        public static string Heal { get { return "SummonerHeal"; } }
        public static string Cleanse { get { return "SummonerBoost"; } }
        public static string Teleport { get { return "SummonerTeleport"; } }
        public static string Barrier { get { return "SummonerBarrier"; } }
        public static string Ignite { get { return "SummonerDot"; } }
    }
    public class Spells
    {
        //public static Spell Q { get { return SpellManager.SpellList.Where(O => O.Key == "Q").FirstOrDefault(); } }
        //public static Spell W { get { return SpellManager.SpellList.Where(O => O.Key == "W").FirstOrDefault(); } }
        //public static Spell E { get { return SpellManager.SpellList.Where(O => O.Key == "E").FirstOrDefault(); } }
        //public static Spell R { get { return SpellManager.SpellList.Where(O => O.Key == "R").FirstOrDefault(); } }
        //public static Spell D { get { return SpellManager.SpellList.Where(O => O.Key == "D").FirstOrDefault(); } }
        //public static Spell F { get { return SpellManager.SpellList.Where(O => O.Key == "F").FirstOrDefault(); } }
    }
    public class JungleCreeps
    {
        public class Team
        {
            public class Blue
            {
                public class CampBlue
                {
                    // ReSharper disable once MemberHidesStaticFromOuterClass
                    public static string Blue { get { return "AncientGolem1.1.1"; } }
                    public static string YoungLizard1 { get { return "YoungLizard1.1.2"; } }
                    public static string YoungLizard2 { get { return "YoungLizard1.1.3"; } }
                }

                public class CampWolves
                {
                    public static string GiantWolf { get { return "GiantWolf2.1.3"; } }
                    public static string SmallWolf1 { get { return "wolf2.1.1"; } }
                    public static string SmallWolf2 { get { return "wolf2.1.2"; } }
                }

                public class CampWraiths
                {
                    public static string GiantWraith { get { return "Wraith3.1.3"; } }
                    public static string LesserWraith1 { get { return "Wraith3.1.1"; } }
                    public static string LesserWraith2 { get { return "Wraith3.1.2"; } }
                    public static string LesserWraith3 { get { return "Wraith3.1.3"; } }
                }

                public class CampRed
                {
                    // ReSharper disable once MemberHidesStaticFromOuterClass
                    public static string Red { get { return "LizardElder4.1.1"; } }
                    public static string YoungLizard1 { get { return "YoungLizard4.1.2"; } }
                    public static string YoungLizard2 { get { return "YoungLizard4.1.3"; } }
                }

                public class CampGolems
                {
                    public static string GiantGolem { get { return "Golem5.1.2"; } }
                    public static string SmallGolem { get { return "SmallGolem5.1.1"; } }
                }
            }

            public class Red
            {
                public class CampBlue
                {
                    // ReSharper disable once MemberHidesStaticFromOuterClass
                    public static string Blue { get { return "AncientGolem7.1.1"; } }
                    public static string YoungLizard1 { get { return "YoungLizard7.1.2"; } }
                    public static string YoungLizard2 { get { return "YoungLizard7.1.3"; } }
                }

                public class CampWolves
                {
                    public static string GiantWolf { get { return "GiantWolf8.1.3"; } }
                    public static string SmallWolf1 { get { return "wolf8.1.1"; } }
                    public static string SmallWolf2 { get { return "wolf8.1.2"; } }
                }

                public class CampWraiths
                {
                    public static string GiantWraith { get { return "Wraith9.1.3"; } }
                    public static string LesserWraith1 { get { return "Wraith9.1.1"; } }
                    public static string LesserWraith2 { get { return "Wraith9.1.2"; } }
                    public static string LesserWraith3 { get { return "Wraith9.1.3"; } }
                }

                public class CampRed
                {
                    // ReSharper disable once MemberHidesStaticFromOuterClass
                    public static string Red { get { return "LizardElder10.1.1"; } }
                    public static string YoungLizard1 { get { return "YoungLizard10.1.2"; } }
                    public static string YoungLizard2 { get { return "YoungLizard10.1.3"; } }
                }

                public class CampGolems
                {
                    public static string GiantGolem { get { return "Golem11.1.2"; } }
                    public static string SmallGolem { get { return "SmallGolem11.1.1"; } }
                }
            }

            public class Neutral
            {
                public static string Drake { get { return "Dragon6.1.1"; } }
                public static string Baron { get { return "Worm12.1.1"; } }
            }
        }
    }
    public enum GameState
    {
        Loading = 0,
        ObjectsSpawning = 1,
        InGame = 2,
        GameExiting = 3,
    }
    #region ITEMS
    public enum Items
    {
        AbyssalScepter = 3001,
        AegisOfTheLegion = 3105,
        AmplifyingTome = 1052,
        ArchangelsStaff = 3003,
        AthenesUnholyGrail = 3174,
        AtmasImpaler = 3005,
        AugmentPower = 3196, //Only available for Viktor
        AugmentGravity = 3197, //Only available for Viktor
        AugmentDeath = 3198, //Only available for Viktor
        AvariceBlade = 3093,
        BfSword = 1038,
        BannerOfCommand = 3060,
        BansheesVeil = 3102, //Classic - Replace by Odyn's Veil in Dominion
        BerserkersGreaves = 3006,
        BerserkersGreavesHomeguard = 3250, //Enchantment: Homeguard
        BerserkersGreavesCaptain = 3251, //Enchantment: Captain
        BerserkersGreavesFuror = 3252, //Enchantment: Furor
        BerserkersGreavesDistortion = 3253, //Enchantment: Distortion
        BerserkersGreavesAlacrity = 3254, //Enchantment: Alacrity
        BilgewaterCutlass = 3144,
        BlackCleaver = 3071,
        BlackfireTorch = 3188, //Twisted Treeline
        BladeOfTheRuinedKing = 3153,
        BlastingWand = 1025,
        Bloodthirster = 3072,
        BonetoothNecklace = 3166, //Only available to Rengar
        BootsOfMobility = 3117,
        BootsOfMobilityHomeguard = 3270, //Enchantment: Homeguard
        BootsOfMobilityCaptain = 3271, //Enchantment: Captain
        BootsOfMobilityFuror = 3272, //Enchantment: Furor
        BootsOfMobilityDistortion = 3273, //Enchantment: Distortion
        BootsOfMobilityAlacrity = 3274, //Enchantment: Alacrity
        BootsOfSpeed = 1001,
        BootsOfSwiftness = 3009,
        BootsOfSwiftnessHomeguard = 3280, //Enchantment: Homeguard
        BootsOfSwiftnessCaptain = 3281, //Enchantment: Captain
        BootsOfSwiftnessFuror = 3282, //Enchantment: Furor
        BootsOfSwiftnessDistortion = 3283, //Enchantment: Distortion
        BootsOfSwiftnessAlacrity = 3284, //Enchantment: Alacrity
        BrawlersGloves = 1051,
        Brutalizer = 3134,
        CatalystTheProtector = 3010,
        ChainVest = 1031,
        ChaliceOfHarmony = 3028,
        CloakOfAgility = 1018,
        ClothArmor = 1029,
        CrystallineFlask = 2041,
        Dagger = 1042,
        DeathfireGrasp = 3128,
        DoransBlade = 1055, //Classic - Replaced by Prospector's Blade in Dominion
        DoransRing = 1056, //Classic - Replaced by Prospector's Ring in Dominion
        DoransShield = 1054,
        EleisasMiracle = 3173,
        ElixirOfBrilliance = 2039, //Classic
        ElixirOfFortitude = 2037, //Classic
        EmblemOfValor = 3097,
        Entropy = 3184, //Dominion
        ExecutionersCalling = 3123,
        FaerieCharm = 1004,
        FiendishCodex = 3108,
        FrozenHeart = 3110,
        FrozenMallet = 3022,
        GiantsBelt = 1011,
        GlacialShroud = 3024,
        GrezsSpectralLantern = 3159, //Twisted Treeline
        GuardianAngel = 3026,
        GuinsoosRageblade = 3124,
        HauntingGuise = 3136,
        HealthPotion = 2003,
        HexCore = 3200, //Only available to Viktor
        Hexdrinker = 3155,
        HextechGunblade = 3146,
        HextechRevolver = 3145,
        HextechSweeper = 3187, //Dominion
        HuntersMachete = 1039,
        IcebornGauntlet = 3025,
        IchorOfIllumination = 2048, //Twisted Treeline
        IchorOfRage = 2040, //Twisted Treeline
        InfinityEdge = 3031,
        IonianBootsOfLucidity = 3158,
        IonianBootsOfLucidityHomeguard = 3275, //Enchantment: Homeguard
        IonianBootsOfLucidityCaptain = 3276, //Enchantment: Captain
        IonianBootsOfLucidityFuror = 3277, //Enchantment: Furor
        IonianBootsOfLucidityDistortion = 3278, //Enchantment: Distortion
        IonianBootsOfLucidityAlacrity = 3279, //Enchantment: Alacrity
        KagesLuckyPick = 3098,
        Kindlegem = 3067,
        KitaesBloodrazor = 3186, //Dominion
        LastWhisper = 3035,
        LiandrysTorment = 3151,
        LichBane = 3100,
        Lightbringer = 3185, //Dominion
        LocketOfTheIronSolari = 3190,
        LongSword = 1036,
        LordVanDammsPillager = 3104, //Twisted Treeline
        MadredsBloodrazor = 3126, //Classic - Replaced by Kitae's Bloodrazor in Dominion
        MadredsRazors = 3106, //Classic
        Malady = 3114,
        ManaManipulator = 3037,
        ManaPotion = 2004,
        Manamune = 3004,
        MawOfMalmortius = 3156,
        MejaisSoulstealer = 3041, //Clasic
        MercurialScimitar = 3139,
        MercurysTreads = 3111,
        MercurysTreadsHomeguard = 3265, //Enchantment: Homeguard
        MercurysTreadsCaptain = 3266, //Enchantment: Captain
        MercurysTreadsFuror = 3267, //Enchantment: Furor
        MercurysTreadsDistortion = 3268, //Enchantment: Distortion
        MercurysTreadsAlacrity = 3269, //Enchantment: Alacrity
        MikaelsCrucible = 3222,
        Morellonomicon = 3165,
        Muramana = 3042,
        NashorsTooth = 3115,
        NeedlesslyLargeRod = 1058,
        NegatronCloak = 1057,
        NinjaTabi = 3047,
        NinjaTabiHomeguard = 3260, //Enchantment: Homeguard
        NinjaTabiCaptain = 3261, //Enchantment: Captain
        NinjaTabiFuror = 3262, //Enchantment: Furor
        NinjaTabiDistortion = 3263, //Enchantment: Distortion
        NinjaTabiAlacrity = 3264, //Enchantment: Alacrity
        NullMagicMantle = 1033,
        OdynsVeil = 3180, //Dominion
        Ohmwrecker = 3056,
        OraclesElixir = 2042, //Classic - Replaced by Oracle's Extract in Dominion
        OraclesExtract = 2047, //Dominion
        OverlordsBloodmail = 3084, //Twisted Treeline
        Phage = 3044,
        PhantomDancer = 3046,
        PhilosophersStone = 3096,
        Pickaxe = 1037,
        ProspectorsBlade = 1062, //Dominion
        ProspectorsRing = 1063, //Dominion
        QuicksilverSash = 3140,
        RabadonsDeathcap = 3080, //Replaced by Wooglet's Witchcap in Twisted Treeline and Dominion
        RanduinsOmen = 3143,
        RavenousHydra = 3074, //Melee only
        RecurveBow = 1043,
        RejuvenationBead = 1006,
        RodOfAges = 3027,
        RubyCrystal = 1028,
        RubySightstone = 2045,
        RunaansHurricane = 3085, //Ranged only
        RunicBulwark = 3107,
        RylaisCrystalScepter = 3116,
        SanguineBlade = 3181, //Dominion and Twisted Treeline
        SapphireCrystal = 1027,
        SeekersArmguard = 3191,
        SeraphsEmbrace = 3040,
        ShardOfTrueIce = 3092,
        Sheen = 3057,
        ShurelyasReverie = 3069,
        SightWard = 2044, //Classic
        Sightstone = 2049,
        SorcerersShoes = 3020,
        SorcerersShoesHomeguard = 3255, //Enchantment: Homeguard
        SorcerersShoesCaptain = 3256, //Enchantment: Captain
        SorcerersShoesFuror = 3257, //Enchantment: Furor
        SorcerersShoesDistortion = 3258, //Enchantment: Distortion
        SorcerersShoesAlacrity = 3259, //Enchantment: Alacrity
        SpiritStone = 1080,
        SpiritOfTheAncientGolem = 3207,
        SpiritOfTheElderLizard = 3209,
        SpiritOfTheSpectralWraith = 3206,
        SpiritVisage = 3065,
        StatikkShiv = 3087,
        Stinger = 3101,
        SunfireCape = 3068,
        SwordOfTheDivine = 3131,
        SwordOfTheOccult = 3141, //Classic
        TearOfTheGoddess = 3070,
        Thornmail = 3075,
        Tiamat = 3077,
        TrinityForce = 3078,
        TwinShadows = 3023,
        VampiricScepter = 1053,
        VisionWard = 2043, //Classic
        VoidStaff = 3135,
        WardensMail = 3082,
        WarmogsArmor = 3083, //Classic - Replaced by Overlord's Bloodmail in Twisted Treeline
        WickedHatchet = 3122, //Twisted Treeline
        WillOfTheAncients = 3152,
        WitsEnd = 3091,
        WoogletsWitchcap = 3090, //Twisted Treeline
        WrigglesLantern = 3154, //Classic
        YoumuusGhostblade = 3142,
        Zeal = 3086,
        ZekesHerald = 3050,
        Zephyr = 3172,
        ZhonyasHourglass = 3157,
    }
    #endregion
    #region Champions
    public enum Champions
    {
        [StringValue("Aatrox")]
        Aatrox = 0,
        [StringValue("Ahri")]
        Ahri = 1,
        [StringValue("Akali")]
        Akali = 2,
        [StringValue("Alistar")]
        Alistar = 3,
        [StringValue("Amumu")]
        Amumu = 4,
        [StringValue("Anivia")]
        Anivia = 5,
        [StringValue("Annie")]
        Annie = 6,
        [StringValue("Ashe")]
        Ashe = 7,
        [StringValue("Blitzcrank")]
        Blitzcrank = 8,
        [StringValue("Brand")]
        Brand = 9,
        [StringValue("Caitlyn")]
        Caitlyn = 10,
        [StringValue("Cassiopeia")]
        Cassiopeia = 11,
        [StringValue("Cho'Gath")]
        ChoGath = 12,
        [StringValue("Corki")]
        Corki = 13,
        [StringValue("Darius")]
        Darius = 14,
        [StringValue("Diana")]
        Diana = 15,
        [StringValue("Dr. Mundo")]
        DrMundo = 16,
        [StringValue("Draven")]
        Draven = 17,
        [StringValue("Elise")]
        Elise = 18,
        [StringValue("Evelynn")]
        Evelynn = 19,
        [StringValue("Ezreal")]
        Ezreal = 20,
        [StringValue("Fiddlesticks")]
        Fiddlesticks = 21,
        [StringValue("Fiora")]
        Fiora = 22,
        [StringValue("Fzz")]
        Fizz = 23,
        [StringValue("Galio")]
        Galio = 24,
        [StringValue("Gangplank")]
        Gangplank = 25,
        [StringValue("Garen")]
        Garen = 26,
        [StringValue("Gragas")]
        Gragas = 27,
        [StringValue("Graves")]
        Graves = 28,
        [StringValue("Hecarim")]
        Hecarim = 29,
        [StringValue("Heimerdinger")]
        Heimerdinger = 30,
        [StringValue("Irelia")]
        Irelia = 31,
        [StringValue("Janna")]
        Janna = 32,
        [StringValue("Jarvan IV")]
        JarvanIv = 33,
        [StringValue("Jax")]
        Jax = 34,
        [StringValue("Jayce")]
        Jayce = 35,
        [StringValue("Karma")]
        Karma = 36,
        [StringValue("Karthus")]
        Karthus = 37,
        [StringValue("Kassadin")]
        Kassadin = 38,
        [StringValue("Katarina")]
        Katarina = 39,
        [StringValue("Kayle")]
        Kayle = 40,
        [StringValue("Kennen")]
        Kennen = 41,
        [StringValue("Kha'Zix")]
        KhaZix = 41,
        [StringValue("Kog'Maw")]
        KogMaw = 42,
        [StringValue("LeBlanc")]
        LeBlanc = 43,
        [StringValue("Lee Sin")]
        LeeSin = 44,
        [StringValue("Leona")]
        Leona = 45,
        [StringValue("Lissandra")]
        Lissandra = 46,
        [StringValue("Lucian")]
        Lucian = 47,
        [StringValue("Lulu")]
        Lulu = 48,
        [StringValue("Lux")]
        Lux = 49,
        [StringValue("Malphite")]
        Malphite = 50,
        [StringValue("Malzahar")]
        Malzahar = 51,
        [StringValue("Maokai")]
        Maokai = 52,
        [StringValue("Master Yi")]
        MasterYi = 53,
        [StringValue("Miss Fortune")]
        MissFortune = 54,
        [StringValue("Mordekaiser")]
        Mordekaiser = 55,
        [StringValue("Morgana")]
        Morgana = 56,
        [StringValue("Nami")]
        Nami = 57,
        [StringValue("Nasus")]
        Nasus = 58,
        [StringValue("Nautilus")]
        Nautilus = 59,
        [StringValue("Nidalee")]
        Nidalee = 60,
        [StringValue("Nocturne")]
        Nocturne = 61,
        [StringValue("Nunu")]
        Nunu = 62,
        [StringValue("Olaf")]
        Olaf = 63,
        [StringValue("Orianna")]
        Orianna = 64,
        [StringValue("Pantheon")]
        Pantheon = 65,
        [StringValue("Poppy")]
        Poppy = 66,
        [StringValue("Quinn")]
        Quinn = 67,
        [StringValue("Rammus")]
        Rammus = 68,
        [StringValue("Renekton")]
        Renekton = 69,
        [StringValue("Rengar")]
        Rengar = 70,
        [StringValue("Riven")]
        Riven = 71,
        [StringValue("Rumble")]
        Rumble = 72,
        [StringValue("Ryze")]
        Ryze = 73,
        [StringValue("Sejuani")]
        Sejuani = 74,
        [StringValue("Shaco")]
        Shaco = 75,
        [StringValue("Shen")]
        Shen = 76,
        [StringValue("Shyvana")]
        Shyvana = 77,
        [StringValue("Singed")]
        Singed = 78,
        [StringValue("Sion")]
        Sion = 79,
        [StringValue("Sivir")]
        Sivir = 80,
        [StringValue("Skarner")]
        Skarner = 81,
        [StringValue("Sona")]
        Sona = 82,
        [StringValue("Soraka")]
        Soraka = 83,
        [StringValue("Swain")]
        Swain = 84,
        [StringValue("Syndra")]
        Syndra = 85,
        [StringValue("Talon")]
        Talon = 86,
        [StringValue("Taric")]
        Taric = 87,
        [StringValue("Teemo")]
        Teemo = 88,
        [StringValue("Thresh")]
        Thresh = 89,
        [StringValue("Tristana")]
        Tristana = 90,
        [StringValue("Trundle")]
        Trundle = 91,
        [StringValue("Tryndamere")]
        Tryndamere = 92,
        [StringValue("Twisted Fate")]
        TwistedFate = 93,
        [StringValue("Twitch")]
        Twitch = 94,
        [StringValue("Udyr")]
        Udyr = 95,
        [StringValue("Urgot")]
        Urgot = 96,
        [StringValue("Varus")]
        Varus = 97,
        [StringValue("Vayne")]
        Vayne = 98,
        [StringValue("Veigar")]
        Veigar = 99,
        [StringValue("Vi")]
        Vi = 100,
        [StringValue("Viktor")]
        Viktor = 101,
        [StringValue("Vladimir")]
        Vladimir = 102,
        [StringValue("Volibear")]
        Volibear = 103,
        [StringValue("Warwick")]
        Warwick = 104,
        [StringValue("Wukong")]
        Wukong = 105,
        [StringValue("Xerath")]
        Xerath = 106,
        [StringValue("Xin Zhao")]
        XinZhao = 107,
        [StringValue("Yorick")]
        Yorick = 108,
        [StringValue("Zac")]
        Zac = 109,
        [StringValue("Zed")]
        Zed = 110,
        [StringValue("Ziggs")]
        Ziggs = 111,
        [StringValue("Zilean")]
        Zilean = 112,
        [StringValue("Zyra")]
        Zyra = 113,
    }
    #endregion
}