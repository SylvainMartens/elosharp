﻿namespace EloSharp
{
    abstract class Offsets
    {
        public class ObjectManager
        {
            internal static int FirstObject { get { return 0x1D742B8; } }
            internal static int LastObject { get { return FirstObject + 0x4; } }

            internal static int PlayerManager { get { return 0x10D2DD8; } }
        }
        public class GameObject
        {
            internal static int Name { get { return 0x2C; } }
            internal static int Champion { get { return 0x534; } }
            internal static int Team { get { return 0x1C; } }
            internal static int ObjectName { get { return 0x30; } }
            internal static int Type { get { return 0x3C; } }
            internal static int ClassType { get { return 0x4C; } }
            internal static int Level { get { return 0x4750; } }
            internal static int Position { get { return 0x68; } }

            internal static int Health1 { get { return 0x14; } }//5.15
            internal static int Health2 { get { return 0x0; } }//5.15
            internal static int Health3 { get { return 0x50; } }//5.15

            internal static int MaxHealth1 { get { return 0x14; } }//5.15
            internal static int MaxHealth2 { get { return 0x0; } }//5.15
            internal static int MaxHealth3 { get { return 0x54; } }//5.15

            internal static int Mana1 { get { return 0x14; } }//5.15
            internal static int Mana2 { get { return 0x4; } }//5.15
            internal static int Mana3 { get { return 0x50; } }//5.15

            internal static int MaxMana1 { get { return 0x14; } }//5.15
            internal static int MaxMana2 { get { return 0x4; } }//5.15
            internal static int MaxMana3 { get { return 0x54; } }//5.15

            internal static int BaseDamage1 { get { return 0x1C; } }
            internal static int BaseDamage2 { get { return 0x14; } }
            internal static int BaseDamage3 { get { return 0x10; } }


            internal static int BonusDamage { get { return 0x9E0; } }


            internal static int AttackSpeed1 { get { return 0x1C; } }//5.15
            internal static int AttackSpeed2 { get { return 0x14; } }//5.15
            internal static int AttackSpeed3 { get { return 0x18; } }//5.15

            internal static int AbilityPower1 { get { return 0x1C; } }//5.15
            internal static int AbilityPower2 { get { return 0x14; } }//5.15
            internal static int AbilityPower3 { get { return 0x0; } }//5.15

            internal static int Armor1 { get { return 0x1C; } }//5.15
            internal static int Armor2 { get { return 0x14; } }//5.15
            internal static int Armor3 { get { return 0x8; } }//5.15

            internal static int MagicRessist1 { get { return 0x1c; } }//5.15
            internal static int MagicRessist2 { get { return 0x14; } }//5.15
            internal static int MagicRessist3 { get { return 0x20; } }//5.15

            internal static int MovementSpeed1 { get { return 0x1C; } }//5.15
            internal static int MovementSpeed2 { get { return 0x14; } }//5.15
            internal static int MovementSpeed3 { get { return 0x28; } }//5.15

            internal static int LifeSteal1 { get { return 0x1C; } }//5.15
            internal static int LifeSteal2 { get { return 0x14; } }//5.15
            internal static int LifeSteal3 { get { return 0x28; } }//5.15

            internal static int Critical1 { get { return 0x1C; } }//5.15
            internal static int Critical2 { get { return 0x14; } }//5.15
            internal static int Critical3 { get { return 0x30; } }//5.15

            internal static int CooldownRedution1 { get { return 0x1C; } }//5.15
            internal static int CooldownRedution2 { get { return 0x14; } }//5.15
            internal static int CooldownRedution3 { get { return 0x38; } }//5.15

            internal static int Range { get { return 0x0AF0; } }
            internal static int Inventory { get { return 0xFBC; } }

            internal static int Gold1 { get { return 0x24; } }//5.15
            internal static int Gold2 { get { return 0x44; } }//5.15
        }
        public class Item
        {
            internal static int ItemId { get { return 0x64; } }
            internal static int BuyValue { get { return 0x78; } }
            internal static int Usable { get { return 0x6C; } }
        }
        public class SpellInfo
        {
            internal static int SpellInfoPointer { get { return 0x48; } }
            internal static int ChampionSciSlot { get { return 0x1424; } }
            internal static int ChampionSci { get { return 0x13E8; } }
            internal static int SummonerSci { get { return 0x47A0; } }
            internal static int SummonerSciSlot { get { return 0x47DC; } }
            internal static int InfoPointer { get { return 0x20; } }
            internal static int Name { get { return 0x1C; } }
            internal static int Range { get { return 0x57C; } }
            internal static int Range2 { get { return 0x598; } }
            internal static int IsCasting { get { return 0x74; } }
            internal static int MagicDamage { get { return 0x13C; } }
            internal static int ManaCost { get { return 0x7C8; } }
            internal static int BonusDamage { get { return 0x48; } }
            internal static int PhysicalDamage { get { return 0xCC; } }
            internal static int IsReady { get { return 0x354; } }
            internal static int SkillLevel { get { return 0x8; } }
            internal static int SkillPoints { get { return ChampionSci + 0x18; } }
        }

        public class LoL
        {
            internal static int LocalPlayerName { get { return 0x3A3A134; } }

            internal static int LocalPlayer { get { return 0x10D2DD8; } } //5.15
            internal static int GameState { get { return 0x10CA9C4; } } //5.15

            internal static int SpellCastInfo { get { return 0x03E2BE70; } }
            internal static int CastSpell { get { return 0xBC8E40; } }
            internal static int FloatingFont { get { return 0x3E39B20; } }
            internal static int DrawFloatText { get { return 0xAD9280; } }
            internal static int SkinHack { get { return 0x00BD48F7; } }
            internal static int PrintChatMessage { get { return 0xAE2B10; } }
            internal static int ChatFrame { get { return 0x3E1A978; } }
            internal static int MoveTo { get { return 0xAAC6C0; } }
            internal static int SpellState { get { return 0x00BC9220; } }
            internal static int UpgradeSpell { get { return 0xBC8FD0; } }
            internal static int SendMessage { get { return 0x00B43CB0; } }
            internal static int BuyItem { get { return 0xBDA710; } }
            internal static int SellItem { get { return 0xBD9B70; } }
            internal static int HudTarget { get { return 0x03E2BFAD; } }
            internal static int FindUnit { get { return 0x0043BDF0; } }
            internal static int IsChatOpen { get { return 0x3E1A9F8; } }
            internal static int GameTime { get { return 0xBD2A4; } }
            internal static int SkinHackApply { get { return 0x00BCEF37; } }
            internal static int SkinHackJumpback { get { return 0x00BCEF3D; } }
            internal static int GameId { get { return 0x3E3A128; } }
        }
        public class Camera
        {
            internal static int MaxRange { get { return 0x01976C44; } }
            internal static int Pitch { get { return 0x0184A8BC; } }
            internal static int Pitch2 { get { return 0x01976824; } }
            internal static int Pitch3 { get { return 0x017AE2D8; } }
            internal static int ZoomFactor { get { return 0x0197671C; } }
        }
        public class HeroInventory
        {
            internal static int GetInventoryId { get { return 0x00BD97D0; } }
        }
    }
    public enum Team
    {
        Blue = 100,
        Red = 200,
        Neutral = 300
    }
}