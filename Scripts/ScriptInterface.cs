﻿using System;
using System.Windows.Forms;
namespace EloSharp.Scripts
{
    public abstract class ScriptInterface
    {
        public virtual void OnLoad() { }
        public virtual void OnUnload() { }
        public virtual void OnPulse() { }
        public virtual void OnLoLLoading() { }
        public virtual void ShowForm() { }
        public virtual void OnKeyDown(Keys key) { }
        public abstract string Author { get; }
        public abstract string Name { get; }
        public abstract Version Version { get; }
    }
}