﻿partial class AutoLevelerConfigForm
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
            this.formSkin1 = new FlatTheme.FormSkin();
            this.Btn_Cancel = new FlatTheme.FlatButton();
            this.Btn_Save = new FlatTheme.FlatButton();
            this.formSkin1.SuspendLayout();
            this.SuspendLayout();
            // 
            // formSkin1
            // 
            this.formSkin1.BackColor = System.Drawing.Color.White;
            this.formSkin1.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.formSkin1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(58)))), ((int)(((byte)(60)))));
            this.formSkin1.Controls.Add(this.Btn_Cancel);
            this.formSkin1.Controls.Add(this.Btn_Save);
            this.formSkin1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.formSkin1.FlatColor = System.Drawing.Color.DodgerBlue;
            this.formSkin1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.formSkin1.HeaderColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.formSkin1.HeaderLightColor = System.Drawing.Color.FromArgb(((int)(((byte)(171)))), ((int)(((byte)(171)))), ((int)(((byte)(172)))));
            this.formSkin1.HeaderMaximize = false;
            this.formSkin1.Location = new System.Drawing.Point(0, 0);
            this.formSkin1.Movable = false;
            this.formSkin1.Name = "formSkin1";
            this.formSkin1.Size = new System.Drawing.Size(514, 383);
            this.formSkin1.TabIndex = 2;
            this.formSkin1.Text = "Auto Leveler - Config";
            this.formSkin1.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(234)))), ((int)(((byte)(234)))), ((int)(((byte)(234)))));
            // 
            // Btn_Cancel
            // 
            this.Btn_Cancel.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Cancel.BaseColor = System.Drawing.Color.Red;
            this.Btn_Cancel.ButtonImage = null;
            this.Btn_Cancel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Cancel.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Btn_Cancel.ImageHeight = 0;
            this.Btn_Cancel.ImageLeft = 0;
            this.Btn_Cancel.ImageTop = 0;
            this.Btn_Cancel.ImageWidth = 0;
            this.Btn_Cancel.Location = new System.Drawing.Point(416, 339);
            this.Btn_Cancel.Name = "Btn_Cancel";
            this.Btn_Cancel.OnClickColor = System.Drawing.Color.Red;
            this.Btn_Cancel.OnMouseHover = System.Drawing.Color.Red;
            this.Btn_Cancel.OnMouseUpColor = System.Drawing.Color.Red;
            this.Btn_Cancel.Rounded = false;
            this.Btn_Cancel.Size = new System.Drawing.Size(86, 32);
            this.Btn_Cancel.TabIndex = 2;
            this.Btn_Cancel.Text = "Cancel";
            this.Btn_Cancel.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.Btn_Cancel.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.Btn_Cancel.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            this.Btn_Cancel.Click += new System.EventHandler(this.Btn_Cancel_Click);
            // 
            // Btn_Save
            // 
            this.Btn_Save.BackColor = System.Drawing.Color.Transparent;
            this.Btn_Save.BaseColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(53)))), ((int)(((byte)(53)))));
            this.Btn_Save.ButtonImage = null;
            this.Btn_Save.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Btn_Save.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Btn_Save.ImageHeight = 0;
            this.Btn_Save.ImageLeft = 0;
            this.Btn_Save.ImageTop = 0;
            this.Btn_Save.ImageWidth = 0;
            this.Btn_Save.Location = new System.Drawing.Point(12, 339);
            this.Btn_Save.Name = "Btn_Save";
            this.Btn_Save.OnClickColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(57)))), ((int)(((byte)(51)))));
            this.Btn_Save.OnMouseHover = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.Btn_Save.OnMouseUpColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(57)))), ((int)(((byte)(51)))));
            this.Btn_Save.Rounded = false;
            this.Btn_Save.Size = new System.Drawing.Size(106, 32);
            this.Btn_Save.TabIndex = 1;
            this.Btn_Save.Text = "Save";
            this.Btn_Save.TextColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(243)))));
            this.Btn_Save.TextLeftRightAlign = System.Drawing.StringAlignment.Center;
            this.Btn_Save.TextTopDownAlign = System.Drawing.StringAlignment.Center;
            this.Btn_Save.Click += new System.EventHandler(this.Btn_Save_Click);
            // 
            // AutoLevelerConfigForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(514, 383);
            this.Controls.Add(this.formSkin1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AutoLevelerConfigForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AutoLevelerConfigForm";
            this.TransparencyKey = System.Drawing.Color.Fuchsia;
            this.formSkin1.ResumeLayout(false);
            this.ResumeLayout(false);

    }

    #endregion

    private FlatTheme.FormSkin formSkin1;
    private FlatTheme.FlatButton Btn_Cancel;
    private FlatTheme.FlatButton Btn_Save;
}