﻿using System;
using EloSharp;
using EloSharp.LoLInternals.Objects;
using EloSharp.Managers;
using EloSharp.Scripts;
// ReSharper disable once CheckNamespace
public class AutoLeveler : ScriptInterface
{
    private static readonly GameObject Player = ObjectManager.Me;
    private static bool _enabled;
    private static bool _init;
    public AutoLeveler()
    {
        Init();
    }
    public override string Author
    {
        get { return "Hesa"; }
    }
    public override Version Version
    {
        get { return new Version("1.0.0"); }
    }
    public override string Name
    {
        get { return "AutoLeveler"; }
    }
    private static void Init()
    {
        if (_init)
        {
            return;
        }
        Debug.Log("AutoLeveler Initialized.");
        _init = true;
        //Game.OnUpdate += Game_OnGameUpdate;
    }
// ReSharper disable once UnusedMember.Local
// ReSharper disable once UnusedParameter.Local
    private static void Game_OnGameUpdate(EventArgs args)
    {
        if (!_enabled)
        {
// ReSharper disable once RedundantJumpStatement
            return;
        }
    }
    public static void Enable()
    {
        _enabled = true;
    }
    public static void Disable()
    {
        _enabled = false;
    }
    public static void Enabled(bool b)
    {
        _enabled = b;
    }
    public override void ShowForm()
    {
        new AutoLevelerConfigForm().ShowDialog();
    }
}