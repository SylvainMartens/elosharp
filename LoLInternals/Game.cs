﻿using System;
using System.Diagnostics;
using System.Linq;
using EloSharp.LoLInternals.Objects;
using EloSharp.Managers;
namespace EloSharp.LoLInternals
{
    public class Game
    {
        public class Camera
        {
            public static float MaxRange
            {
                get { return MemoryManager.LoL.Read<float>((IntPtr)Offsets.Camera.MaxRange); }
                set
                {
                    MemoryManager.LoL.Write((IntPtr)Offsets.Camera.MaxRange, value);
                }
            }
            public static float Pitch
            {
                get { return MemoryManager.LoL.Read<float>((IntPtr)Offsets.Camera.Pitch); }
                set
                {
                    MemoryManager.LoL.Write((IntPtr)Offsets.Camera.Pitch, value);
                }
            }
            public static float Pitch2
            {
                get { return MemoryManager.LoL.Read<float>((IntPtr)Offsets.Camera.Pitch2); }
                set
                {
                    MemoryManager.LoL.Write((IntPtr)Offsets.Camera.Pitch2, value);
                }
            }
            public static float Pitch3
            {
                get { return MemoryManager.LoL.Read<float>((IntPtr)Offsets.Camera.Pitch3); }
                set
                {
                    MemoryManager.LoL.Write((IntPtr)Offsets.Camera.Pitch3, value);
                }
            }
            public static float ZoomFactor
            {
                get { return MemoryManager.LoL.Read<float>((IntPtr)Offsets.Camera.ZoomFactor); }
                set
                {
                    MemoryManager.LoL.Write((IntPtr)Offsets.Camera.ZoomFactor, value);
                }
            }
        }
        public class Time
        {
            public static int Minutes
            {
                get
                {
                    return TimeSpan.FromMilliseconds(MemoryManager.LoL.Read<int>(FModex + 0xBD2A4) - 14000).Minutes;
                }
            }
            public static int Seconds
            {
                get
                {
                    return TimeSpan.FromMilliseconds(MemoryManager.LoL.Read<int>(FModex + 0xBD2A4) - 14000).Seconds;
                }
            }
        }
        public static bool IsChatOpen
        {
            get
            {
                return MemoryManager.Base.Read<bool>((IntPtr)Offsets.LoL.IsChatOpen);
            }
        }
        public static GameObject Target
        {
            get
            {
                using (var memory = new AllocatedMemory(4))
                {
                    var executor = new Executor();
                    lock (executor.AssemblyLock)
                    {
                        executor.AddLine("push {0}", TargetHash);
                        executor.AddLine("call {0}", Offsets.LoL.FindUnit);
                        executor.AddLine("mov [{0}],eax", memory.Address);
                        executor.AddLine("ret");
                        executor.Execute();
                    }
                    return new GameObject(memory.Address);
                }
            }
        }
        public static void DoEmote(EmoteType e)
        {
            Chat.Send(StringEnum.GetStringValue(e));
        }
        public static void Whisper(string friend, string message)
        {
            Chat.Send(string.Format("/w {0} {1}", friend, message));
        }
        public static void Ignore(string name)
        {
            Chat.Send(string.Format("/ignore {0}", name));
        }
        private static IntPtr TargetHash { get { return MemoryManager.Base.Read<IntPtr>((IntPtr)Offsets.LoL.HudTarget); } }
        private static IntPtr FModex { get { return (from ProcessModule m in MemoryManager.LoL.Process.Modules where m.ModuleName == "fmodex.dll" select m).First().BaseAddress; } }
    }
}
