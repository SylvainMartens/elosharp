﻿using System;
using System.Text;
using EloSharp.Managers;
namespace EloSharp.LoLInternals
{
    public class Spell
    {
        public IntPtr BaseAddress { get; set; }
        private int Index { get; set; }
        private readonly bool _isSummoner;

        readonly string[] _chars1 = { "Q", "W", "E", "R" };
        readonly string[] _chars2 = { "D", "F" };

        public Spell(IntPtr baseAddress, int index, bool isSummoner)
        {
            BaseAddress = baseAddress;
            Index = index;
            _isSummoner = isSummoner;
        }
        public string Name
        {
            get
            {
                return MemoryManager.Base.ReadString(MemoryManager.Base.Read<IntPtr>(MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.SpellInfo.InfoPointer) + 0xC) + 0x8, Encoding.UTF8);
            }
        }
        public float Range
        {
            get
            {
// ReSharper disable CompareOfFloatsByEqualityOperator
                if (MemoryManager.Base.Read<float>(MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.SpellInfo.InfoPointer) + Offsets.SpellInfo.Range) == 25000.0f || MemoryManager.Base.Read<float>(MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.SpellInfo.InfoPointer) + Offsets.SpellInfo.Range) == 25000.0f)
                    return MemoryManager.Base.Read<float>(MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.SpellInfo.InfoPointer) + Offsets.SpellInfo.Range2);
// ReSharper disable once RedundantIfElseBlock
                else
                    return MemoryManager.Base.Read<float>(MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.SpellInfo.InfoPointer) + Offsets.SpellInfo.Range);
            }
        }
        public bool IsReady
        {
            get
            {
                return (SpellState == SpellState.Ready);
            }
        }
        public SpellState SpellState
        {
            get
            {
                using (var memory = new AllocatedMemory(4))
                {
                    var executor = new Executor();
                    lock (executor.AssemblyLock)
                    {
                        executor.AddLine(string.Format("pxor xmm0, xmm0"));
                        executor.AddLine(string.Format("push {0}", memory.Address));
                        executor.AddLine(string.Format("push {0}", Index));
                        executor.AddLine(string.Format("xor edi, edi"));
                        executor.AddLine(string.Format("xor ebx, ebx"));
                        executor.AddLine(string.Format("xor eax, eax"));

                        executor.AddLine(IsSummoner
                            ? string.Format("mov ecx, {0}",
                                ObjectManager.Me.BaseAddress + Offsets.SpellInfo.SummonerSci)
                            : string.Format("mov ecx, {0}",
                                ObjectManager.Me.BaseAddress + Offsets.SpellInfo.ChampionSci));

                        executor.AddLine(string.Format("xor edx, edx"));
                        executor.AddLine(string.Format("mov ebp, {0}", Index));
                        executor.AddLine(string.Format("call {0}", Offsets.LoL.SpellState));
                        executor.AddLine(string.Format("mov [{0}], eax", memory.Address));
                        executor.AddLine(string.Format("ret"));
                        executor.Execute();
                    }

                    return (SpellState)memory.Read<int>();
                }
            }
        }
        public float ManaCost
        {
            get
            {
                return MemoryManager.Base.Read<float>(MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.SpellInfo.InfoPointer) + Offsets.SpellInfo.ManaCost);
            }
        }
        public float BonusDamage
        {
            get
            {
                return MemoryManager.Base.Read<float>(BaseAddress + Offsets.SpellInfo.BonusDamage);
            }
        }
        public float PhysicalDamage
        {
            get
            {
                return MemoryManager.Base.Read<float>(MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.SpellInfo.InfoPointer) + Offsets.SpellInfo.PhysicalDamage);
            }
        }
        public float MagicDamage
        {
            get
            {
                return MemoryManager.Base.Read<float>(MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.SpellInfo.InfoPointer) + Offsets.SpellInfo.MagicDamage);
            }
        }
        public bool IsCasting
        {
            get
            {
                return (MemoryManager.Base.Read<byte>(BaseAddress + Offsets.SpellInfo.IsCasting) != 0);
            }
        }
        public int SkillLevel
        {
            get
            {
                return MemoryManager.Base.Read<int>(BaseAddress + Offsets.SpellInfo.SkillLevel);
            }
        }
        public string Key
        {
            get
            {
                return _isSummoner ? _chars2[Index] : _chars1[Index];
            }
        }
        public bool IsSummoner
        {
            get
            {
                return _isSummoner;
            }
        }
        public int Slot
        {
            get { return Index; }
        }
    }
}
