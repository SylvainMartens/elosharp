﻿using System;
using EloSharp.Managers;
namespace EloSharp.LoLInternals.Objects
{
    public class Minion : GameObject
    {
        public Minion(IntPtr baseAddress, bool rebased = false) : base(baseAddress, rebased) { }

        public bool IsEnemy { get { return Team != ObjectManager.Me.Team; } }

        public bool IsNeutralBossMonster { get { return Name == JungleCreeps.Team.Neutral.Baron || Name == JungleCreeps.Team.Neutral.Drake; } }

        public int Level { get { return MemoryManager.Base.Read<byte>(BaseAddress + Offsets.GameObject.Level); } }

        public float Health
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.Health1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.Health2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.Health3);
                return address4;
            }
        }

        public float MaxHealth
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.MaxHealth1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.MaxHealth2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.MaxHealth3);
                return address4;
            }
        }

        public double HealthPercentage { get { return Health.Equals(0) ? 0 : Math.Round(Health * 100 / MaxHealth, 2); } }

        public float Mana
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.Mana1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.Mana2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.Mana3);
                return address4;
            }
        }

        public float MaxMana
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.MaxMana1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.MaxMana2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.MaxMana3);
                return address4;
            }
        }

        public double ManaPercentage { get { return Mana.Equals(0) ? 0 : Math.Round(Mana * 100 / MaxMana, 2); } }

        public float BaseDamage
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.BaseDamage1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.BaseDamage2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.BaseDamage3);
                return address4;
            }
        }

        public float BonusDamage { get { return MemoryManager.Base.Read<float>(BaseAddress + Offsets.GameObject.BonusDamage); } }

        public float AttackSpeed
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.AttackSpeed1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.AttackSpeed2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.AttackSpeed3);
                return address4;
            }
        }

        public float AbilityPower
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.AbilityPower1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.AbilityPower2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.AbilityPower3);
                return address4;
            }
        }
        public float ArmorRessistance
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.Armor1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.Armor2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.Armor3);
                return address4;
            }
        }
        public float MagicRessistance
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.MagicRessist1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.MagicRessist2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.MagicRessist3);
                return address4;
            }
        }

        public float MovementSpeed
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.MovementSpeed1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.MovementSpeed2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.MovementSpeed3);
                return address4;
            }
        }

        public float Range { get { return MemoryManager.Base.Read<float>(BaseAddress + Offsets.GameObject.Range); } }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValidTarget
        {
            get
            {
                if (Health.Equals(0)) return false;
                return BaseAddress != ObjectManager.Me.BaseAddress && IsEnemy;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsEnemyBuff
        {
            get
            {
                if (ObjectManager.Me.Team == Team.Red && (Name == JungleCreeps.Team.Blue.CampBlue.Blue || Name == JungleCreeps.Team.Blue.CampRed.Red))
                    return true;
                return ObjectManager.Me.Team == Team.Blue && (Name == JungleCreeps.Team.Red.CampRed.Red || Name == JungleCreeps.Team.Red.CampBlue.Blue);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool CanSmite { get { if (Health.Equals(0)) return false; return (460 + (ObjectManager.Me.Level * 30) >= Health); } }
    }
}