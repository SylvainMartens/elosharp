﻿using System;
using EloSharp.Managers;
namespace EloSharp.LoLInternals.Objects
{
    public class Turret : GameObject
    {
        public Turret(IntPtr baseAddress, bool rebased = false) : base(baseAddress, rebased) { }

        public float Health
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.Health1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.Health2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.Health3);
                return address4;
            }
        }

        public float MaxHealth
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.MaxHealth1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.MaxHealth2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.MaxHealth3);
                return address4;
            }
        }

        public float ArmorRessistance
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.Armor1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.Armor2);
                var address4 = (float)MemoryManager.Base.Read<IntPtr>(address3 + Offsets.GameObject.Armor3);
                return address4;
                //return MemoryManager.Base.Read<float>(BaseAddress + Offsets.GameObject.ArmorRessistance);
            }
        }

        public float MagicRessistance
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.MagicRessist1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.MagicRessist2);
                var address4 = (float)MemoryManager.Base.Read<IntPtr>(address3 + Offsets.GameObject.MagicRessist3);
                return address4;
                //return MemoryManager.Base.Read<float>(BaseAddress + Offsets.GameObject.MagicRessist);
            }
        }
    }
}
