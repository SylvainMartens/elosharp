﻿using System;
using System.Text;
using EloSharp.Managers;
namespace EloSharp.LoLInternals.Objects
{
    public class GameObject
    {
        public IntPtr BaseAddress { get; set; }

        public GameObject(IntPtr baseAddress, bool rebased = false)
        {
            BaseAddress = !rebased ? MemoryManager.Base.Read<IntPtr>(baseAddress, true) : baseAddress;
        }
        public string Name
        {
            get
            {
                try
                {
                    if (MemoryManager.Base.Read<int>(BaseAddress + Offsets.GameObject.Name) > 0x60000000)
                    {
                        return "Unknown";
                    }

                    if (ObjectType == ObjectType.Player || ObjectType == ObjectType.VisionWard || ObjectType == ObjectType.SightWard)
                        return MemoryManager.Base.ReadString(BaseAddress + Offsets.GameObject.Name, Encoding.UTF8);
                    return MemoryManager.Base.ReadString(MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.Name), Encoding.UTF8);
                }
                catch (Exception)
                {
                    return "Unknown";
                }
            }
        }

        public Vector3 Position { get { return MemoryManager.Base.Read<Vector3>(BaseAddress + Offsets.GameObject.Position); } }
        public double Distance { get { return Math.Round(Math.Sqrt(Math.Pow(Math.Abs(ObjectManager.Me.Position.X - Position.X), 2) + Math.Pow(Math.Abs(ObjectManager.Me.Position.Y - Position.Y), 2) + Math.Pow(Math.Abs(ObjectManager.Me.Position.Z - Position.Z), 2)), 2); } }
        public Team Team { get { return (Team)MemoryManager.Base.Read<int>(BaseAddress + Offsets.GameObject.Team); } }

        public ObjectType ObjectType
        {
            get
            {
                var type = (ObjectType)MemoryManager.Base.Read<byte>(BaseAddress + Offsets.GameObject.Type);
                if (Team == Team.Neutral) return ObjectType.Creeps;
                if (type == ObjectType.Player && MemoryManager.Base.Read<byte>(BaseAddress + 0x40) == 15) return ObjectType.Player;
                if (type == ObjectType.Player && MemoryManager.Base.Read<byte>(BaseAddress + 0x40) == 47) return ObjectType.Unknown;

                return type;
            }
        }
    }
}
