﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EloSharp.Managers;
namespace EloSharp.LoLInternals.Objects
{
    public class Player : GameObject
    {
        public Player(IntPtr baseAddress, bool rebased = false) : base(baseAddress, rebased) { }
        public bool IsEnemy { get { return Team != ObjectManager.Me.Team; } }
        public int Level { get { return MemoryManager.Base.Read<byte>(BaseAddress + Offsets.GameObject.Level); } }

        public float Health
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.Health1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.Health2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.Health3);
                return address4;
            }
        }
        public float MaxHealth
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.MaxHealth1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.MaxHealth2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.MaxHealth3);
                return address4;
            }
        }
        public double HealthPercentage
        {
            get
            {
                return Health.Equals(0) ? 0 : Math.Round(Health * 100 / MaxHealth, 2);
            }
        }
        public float Mana
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.Mana1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.Mana2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.Mana3);
                return address4;
            }
        }
        public float MaxMana
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.MaxMana1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.MaxMana2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.MaxMana3);
                return address4;
            }
        }
        public double ManaPercentage
        {
            get
            {
                return Mana.Equals(0) ? 0 : Math.Round(Mana * 100 / MaxMana, 2);
            }
        }

        public float BaseDamage//Attack Damage ???
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.BaseDamage1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.BaseDamage2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.BaseDamage3);
                return address4;
            }
        }

        public float BonusDamage { get { return MemoryManager.Base.Read<float>(BaseAddress + Offsets.GameObject.BonusDamage); } }

        public float AttackSpeed
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.AttackSpeed1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.AttackSpeed2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.AttackSpeed3);
                return address4;
            }
        }
        public float AbilityPower
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.AbilityPower1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.AbilityPower2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.AbilityPower3);
                return address4;
            }
        }
        public float Armor
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.Armor1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.Armor2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.Armor3);
                return address4;
            }
        }
        public float MagicRessist
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.MagicRessist1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.MagicRessist2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.MagicRessist3);
                return address4;
            }
        }
        public float MovementSpeed
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.MovementSpeed1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.MovementSpeed2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.MovementSpeed3);
                return address4;
            }
        }
        public float LifeSteal
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.LifeSteal1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.LifeSteal2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.LifeSteal3);
                return address4;
            }
        }
        public float Critical
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.Critical1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.Critical2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.Critical3);
                return address4;
            }
        }
        public float CooldownRedution
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.CooldownRedution1);
                var address3 = MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.CooldownRedution2);
                var address4 = MemoryManager.Base.Read<float>(address3 + Offsets.GameObject.CooldownRedution3);
                return Convert.ToInt32(address4 * 100);
            }
        }

        public float Range { get { return MemoryManager.Base.Read<float>(BaseAddress + Offsets.GameObject.Range); } }

        public int Gold
        {
            get
            {
                var address2 = MemoryManager.Base.Read<IntPtr>(BaseAddress + Offsets.GameObject.Gold1);
                var address3 = (float) MemoryManager.Base.Read<IntPtr>(address2 + Offsets.GameObject.Gold2);
                return Convert.ToInt32(address3);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Champions Champion
        {
            get
            {
                return (from Champions c in Enum.GetValues(typeof(Champions)) where StringEnum.GetStringValue(c) == MemoryManager.Base.ReadString(BaseAddress + Offsets.GameObject.Champion, Encoding.UTF8) select c).FirstOrDefault();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValidTarget
        {
            get
            {
                if (Health.Equals(0)) return false;
                return BaseAddress != ObjectManager.Me.BaseAddress && IsEnemy;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public InventoryManager Inventory { get { return new InventoryManager(BaseAddress); } }

        public class InventoryManager
        {
            public IntPtr BaseAddress { get; set; }
            public List<Item> ItemList = new List<Item>();
            public InventoryManager(IntPtr address)
            {
                BaseAddress = address + Offsets.GameObject.Inventory;
                Initialize();
            }
            private void Initialize()
            {
                lock (ItemList)
                {
                    for (int i = 0; i <= 1; i++)
                    {
                        var item = MemoryManager.Base.Read<IntPtr>(BaseAddress + i * 4 + 0x54);
                        var itemArray = MemoryManager.Base.Read<IntPtr>(item + 0x0);
                        if ((int)itemArray == 0) break; //No item on that slot.
                        var itemInfo = MemoryManager.Base.Read<IntPtr>(itemArray + 0x4);
                        ItemList.Add(new Item(itemInfo, i));
                    }
                }
            }
            public Item GetItemBySlot(int slot)
            {
                return slot < 0 ? null : (ItemList.FirstOrDefault(o => o.Index == slot));
            }
            public bool HasItem(int itemId)
            {
                return (ItemList.FirstOrDefault(o => o.ItemId == itemId) != null);
            }
        }
    }
}
