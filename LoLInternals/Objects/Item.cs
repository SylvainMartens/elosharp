﻿using System;
using EloSharp.Managers;
namespace EloSharp.LoLInternals.Objects
{
    public class Item
    {
        public IntPtr BaseAddress { get; set; }
        private int EnumerateIndex { get; set; }
        public Item(IntPtr address, int index)
        {
            BaseAddress = address;
            EnumerateIndex = index;
        }
        public int Index { get { return EnumerateIndex; } }
        public int ItemId { get { return MemoryManager.Base.Read<int>(BaseAddress + Offsets.Item.ItemId); } }
        public int BuyValue { get { return MemoryManager.Base.Read<int>(BaseAddress + Offsets.Item.BuyValue); } }
        public int ChargesLeft { get { return MemoryManager.Base.Read<int>(BaseAddress + 0x64); } }
        public int MaxCharges { get { return MemoryManager.Base.Read<int>(BaseAddress + 0x64); } }
        public bool Usable { get { return (MemoryManager.Base.Read<int>(BaseAddress + Offsets.Item.Usable) > 0); } }

        public ItemState ItemState
        {
            get
            {
                if (!Usable) return 0;
                using (var memory = new AllocatedMemory(4))
                {
                    var executor = new Executor();
                    lock (executor.AssemblyLock)
                    {
                        executor.AddLine(string.Format("pxor xmm0, xmm0"));
                        executor.AddLine(string.Format("push {0}", memory.Address));
                        executor.AddLine(string.Format("push {0}", Index));
                        executor.AddLine(string.Format("xor edi, edi"));
                        executor.AddLine(string.Format("xor ebx, ebx"));
                        executor.AddLine(string.Format("xor eax, eax"));
                        executor.AddLine(string.Format("mov ecx, {0}", ObjectManager.Me.BaseAddress + Offsets.SpellInfo.SummonerSci));
                        executor.AddLine(string.Format("xor edx, edx"));
                        executor.AddLine(string.Format("mov ebp, {0}", Index + 3));
                        executor.AddLine(string.Format("call {0}", Offsets.LoL.SpellState));
                        executor.AddLine(string.Format("mov [{0}], eax", memory.Address));
                        executor.AddLine(string.Format("ret"));
                        executor.Execute();
                    }
                    return (ItemState)memory.Read<int>();
                }
            }
        }
    }
}
