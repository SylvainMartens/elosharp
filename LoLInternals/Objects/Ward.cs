﻿using System;
using EloSharp.Managers;
namespace EloSharp.LoLInternals.Objects
{
    public class Ward : GameObject
    {
        public Ward(IntPtr baseAddress, bool rebased = false) : base(baseAddress, rebased) { }
        public bool IsEnemy { get { return Team != ObjectManager.Me.Team; } }
    }
}
