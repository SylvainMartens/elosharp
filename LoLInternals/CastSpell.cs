﻿using System;
using System.Linq;
using EloSharp.LoLInternals.Objects;
using EloSharp.Managers;
namespace EloSharp.LoLInternals
{
    public class CastSpell
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="spell"></param>
        /// <param name="unit"></param>
        /// <param name="spellName"></param>
        /// l(BASEADDR fai
        internal static bool Cast(Vector3 position, Spell spell, GameObject unit = null, string spellName = "")
        {
            if (!MemoryManager.IsInitialized) { Debug.Log("Error: Tried to cast a spell, but Memory not initialized yet.", MessageTypes.Error); return false; }
            if (MemoryManager.GameState != GameState.InGame) { Debug.Log("Error: Tried to cast a spell, but not ingame yet.", MessageTypes.Error); return false; }

            try
            {
                //string dbg = spell.Name;
            }
            catch (Exception)
            {
                Debug.Log(string.Format("Error: Couldn't find spell '{0}' in your spellbook.", spellName));
                return false;
            }
            if (spell.ManaCost > ObjectManager.Me.Mana)
            {
                Debug.Log(string.Format("Error: Tried to cast '{0}', but we don't have enough mana ({1}/{2}.", spell.Name, ObjectManager.Me.Mana, spell.ManaCost));
                return false;
            }
            var firstOrDefault = SpellManager.SpellList.FirstOrDefault(o => o == spell);
            if (firstOrDefault != null && firstOrDefault.SkillLevel == 0)
            {
                Debug.Log(string.Format("Error: Tried to cast '{0}', which we haven't learned yet.", spell.Name), MessageTypes.Error);
                return false;
            }
            Debug.Log(string.Format("CastSpell: {0}", spell.Name));
            using (var memory = new AllocatedMemory(0xC))
            {
                var executor = new Executor();
                lock (executor.AssemblyLock)
                {
                    memory.Write(position);
                    if (unit != null)
                    {
                        memory.Address = unit.BaseAddress + 0x68;
                    }
                    else
                    {
                        MemoryManager.LoL.Write(memory.Address, position);
                    }
                    executor.AddLine("mov eax,{0}", ObjectManager.Me.BaseAddress);
                    executor.AddLine("add eax,{0}", spell.IsSummoner ? Offsets.SpellInfo.SummonerSciSlot : Offsets.SpellInfo.ChampionSciSlot);
                    executor.AddLine("mov ecx,{0}", spell.Slot);
                    executor.AddLine("mov [eax],ecx");
                    executor.AddLine("add eax,{0}", 0x4);
                    executor.AddLine("mov ecx,{0}", spell.Slot);
                    executor.AddLine("mov [eax],ecx");
                    if (unit == null)
                    {
                        executor.AddLine("push 0");
                        executor.AddLine("push 0");
                        executor.AddLine("push {0}", memory.Address);
                        executor.AddLine("push {0}", memory.Address);
                        executor.AddLine("sub eax,{0}", 0x40);
                        executor.AddLine("mov edi,eax");
                        executor.AddLine("xor ecx,ecx");
                        executor.AddLine("mov edx,{0}", memory.Address);
                        executor.AddLine("mov ebx,{0}", Offsets.LoL.SpellCastInfo);
                        executor.AddLine("mov esi,{0}", ObjectManager.Me.BaseAddress);
                    }
                    else
                    {
                        var pUnit = MemoryManager.Base.Read<IntPtr>(unit.BaseAddress + 0xFC);
                        executor.AddLine("push 0");
                        executor.AddLine("push {0}", pUnit);
                        executor.AddLine("push {0}", memory.Address);
                        executor.AddLine("push {0}", memory.Address);
                        executor.AddLine("sub eax,{0}", 0x40);
                        executor.AddLine("mov edi,eax");
                        executor.AddLine("mov edx,{0}", memory.Address);
                        executor.AddLine("mov ebx,{0}", Offsets.LoL.SpellCastInfo);
                        executor.AddLine("mov ecx,{0}", pUnit);
                        executor.AddLine("mov esi,{0}", ObjectManager.Me.BaseAddress);
                    }
                    executor.AddLine("call {0}", Offsets.LoL.CastSpell);
                    executor.AddLine("ret");
                    executor.Execute();
                }
                return true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="unit"></param>
        public static bool ByName(string name, GameObject unit = null)
        {
            return Cast(ObjectManager.Me.Position, SpellManager.SpellList.FirstOrDefault(o => o.Name == name), unit, name);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="name"></param>>>>>>>>>>>>>>>>
        public static bool ByName(string name, Vector3 position)
        {
            return Cast(position, SpellManager.SpellList.FirstOrDefault(o => o.Name == name), null, name);
        }
        internal static void Recall()
        {
            if (!MemoryManager.IsInitialized) { Debug.Log("Error: Tried to use an item, but Memory not initialized yet.", MessageTypes.Error); return; }
            if (MemoryManager.GameState != GameState.InGame) { Debug.Log("Error: Tried to use item, but not ingame yet.", MessageTypes.Error); return; }

            using (var memory = new AllocatedMemory(0xC))
            {
                var executor = new Executor();
                lock (executor.AssemblyLock)
                {
                    executor.AddLine("mov eax,{0}", ObjectManager.Me.BaseAddress);
                    executor.AddLine("add eax,{0}", Offsets.SpellInfo.ChampionSciSlot);
                    executor.AddLine("mov ecx,{0}", 10);
                    executor.AddLine("mov [eax],ecx");
                    executor.AddLine("add eax,{0}", 0x4);
                    executor.AddLine("mov ecx,{0}", 10);
                    executor.AddLine("mov [eax],ecx");
                    executor.AddLine("push 0");
                    executor.AddLine("push 0");
                    executor.AddLine("push {0}", memory.Address);
                    executor.AddLine("push {0}", memory.Address);
                    executor.AddLine("sub eax,{0}", 0x40);
                    executor.AddLine("mov edi,eax");
                    executor.AddLine("xor ecx,ecx");
                    executor.AddLine("mov edx,{0}", memory.Address);
                    executor.AddLine("mov ebx,{0}", Offsets.LoL.SpellCastInfo);
                    executor.AddLine("mov esi,{0}", ObjectManager.Me.BaseAddress);
                    executor.AddLine("call {0}", Offsets.LoL.CastSpell);
                    executor.AddLine("ret");
                    executor.Execute();
                }
            }
        }
    }
}
