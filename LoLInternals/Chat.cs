﻿namespace EloSharp.LoLInternals
{
    public class Chat
    {
        public static void PrintChatMessage(string Text, int Type = 0)
        {
            using (var memory = new AllocatedMemory(4))
            {
                var executor = new Executor();
                lock (executor.AssemblyLock)
                {
                    memory.WriteString(Text);
                    executor.AddLine("mov ecx, {0}", Offsets.LoL.ChatFrame);
                    executor.AddLine("push {0}", Type);
                    executor.AddLine("push {0}", memory.Address);
                    executor.AddLine("call {0}", Offsets.LoL.PrintChatMessage);
                    executor.AddLine("ret");
                    executor.Execute();
                }
            }
        }
        public static void Send(string Text)
        {
            using (var memory = new AllocatedMemory(4))
            {
                var executor = new Executor();
                lock (executor.AssemblyLock)
                {
                    memory.WriteString(Text);
                    executor.AddLine("push {0}", memory.Address);
                    executor.AddLine("call {0}", Offsets.LoL.SendMessage);
                    executor.AddLine("ret");
                    executor.Execute();
                }
            }
        }
    }
}
