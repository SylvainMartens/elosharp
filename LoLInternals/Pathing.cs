﻿using EloSharp.LoLInternals.Objects;
using EloSharp.Managers;
namespace EloSharp.LoLInternals
{
    public class Pathing
    {
        public static void MoveTo(Vector3 position, MoveType type = MoveType.Move, GameObject unit = null)
        {
            using (var memory = new AllocatedMemory(0xC))
            {
                var executor = new Executor();
                lock (executor.AssemblyLock)
                {
                    memory.Write(position);
                    executor.AddLine("push 1");
                    executor.AddLine("push {0}", 0);
                    executor.AddLine("push {0}", 0);
                    if (unit != null)
                    {
                        executor.AddLine("push {0}", unit.BaseAddress);
                        executor.AddLine("push {0}", unit.BaseAddress + 0x68);
                    }
                    else
                    {
                        executor.AddLine("push {0}", 0);
                        executor.AddLine("push {0}", memory.Address);
                    }
                    executor.AddLine("push {0}", (int)type);
                    executor.AddLine("mov ecx,{0}", ObjectManager.Me.BaseAddress);
                    executor.AddLine("call  {0}", Offsets.LoL.MoveTo);
                    executor.AddLine("ret");
                    executor.Execute();
                }
            }
        }
        public static void FaceTarget(Vector3 position, GameObject unit = null)
        {
            MoveTo(position, MoveType.MoveStop, unit);
            MoveTo(position, MoveType.Move, unit);
            MoveTo(position, MoveType.MoveStop, unit);
        }
    }
}
