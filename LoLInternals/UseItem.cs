﻿using System;
using EloSharp.LoLInternals.Objects;
using EloSharp.Managers;
namespace EloSharp.LoLInternals
{
    public class UseItem
    {
        internal static void BySlot(int slot, Vector3 position = new Vector3(), GameObject target = null)
        {
            var item = ObjectManager.Me.Inventory.GetItemBySlot(slot);
            if (item == null) { Debug.Log("Error: No item found on slot {0}", MessageTypes.Error); return; }
            if (!MemoryManager.IsInitialized) { Debug.Log("Error: Tried to use an item, but Memory not initialized yet.", MessageTypes.Error); return; }
            if (MemoryManager.GameState != GameState.InGame) { Debug.Log("Error: Tried to use item, but not ingame yet.", MessageTypes.Error); return; }
            if (!item.Usable || item.ItemState != ItemState.Ready) { Debug.Log("Error: Item not usable / not ready.", MessageTypes.Error); return; }
            using (var memory = new AllocatedMemory(0xC))
            {
                var executor = new Executor();
                lock (executor.AssemblyLock)
                {
                    memory.Write(position);

                    if (target != null)
                    {
                        memory.Address = target.BaseAddress + 0x68;
                    }
                    else
                    {
                        MemoryManager.LoL.Write(memory.Address, position);
                    }
                    executor.AddLine("mov eax,{0}", ObjectManager.Me.BaseAddress);
                    executor.AddLine("add eax,{0}", Offsets.SpellInfo.ChampionSciSlot);
                    executor.AddLine("mov ecx,{0}", 4 + slot);
                    executor.AddLine("mov [eax],ecx");
                    executor.AddLine("add eax,{0}", 0x4);
                    executor.AddLine("mov ecx,{0}", 4 + slot);
                    executor.AddLine("mov [eax],ecx");
                    if (target == null)
                    {
                        executor.AddLine("push 0");
                        executor.AddLine("push 0");
                        executor.AddLine("push {0}", memory.Address);
                        executor.AddLine("push {0}", memory.Address);
                        executor.AddLine("sub eax,{0}", 0x40);
                        executor.AddLine("mov edi,eax");
                        executor.AddLine("xor ecx,ecx");
                        executor.AddLine("mov edx,{0}", memory.Address);
                        executor.AddLine("mov ebx,{0}", Offsets.LoL.SpellCastInfo);
                        executor.AddLine("mov esi,{0}", ObjectManager.Me.BaseAddress);
                    }
                    else
                    {
                        var pUnit = MemoryManager.Base.Read<IntPtr>(target.BaseAddress + 0xFC);
                        executor.AddLine("push 0");
                        executor.AddLine("push {0}", pUnit);
                        executor.AddLine("push {0}", memory.Address);
                        executor.AddLine("push {0}", memory.Address);
                        executor.AddLine("sub eax,{0}", 0x40);
                        executor.AddLine("mov edi,eax");
                        executor.AddLine("mov edx,{0}", memory.Address);
                        executor.AddLine("mov ebx,{0}", Offsets.LoL.SpellCastInfo);
                        executor.AddLine("mov ecx,{0}", pUnit);
                        executor.AddLine("mov esi,{0}", ObjectManager.Me.BaseAddress);
                    }
                    executor.AddLine("call {0}", Offsets.LoL.CastSpell);
                    executor.AddLine("ret");
                    executor.Execute();
                }
            }
        }
    }
}
