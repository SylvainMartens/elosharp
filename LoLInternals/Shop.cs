﻿using System;
using EloSharp.Managers;
namespace EloSharp.LoLInternals
{
    public class Shop
    {
        public static bool BuyItem(int itemId)
        {
            using (var memory = new AllocatedMemory(4))
            {
                var executor = new Executor();
                lock (executor.AssemblyLock)
                {
                    executor.AddLine("mov eax, {0}", itemId);
                    executor.AddLine("mov edi, {0}", ObjectManager.Me.Inventory.BaseAddress);
                    executor.AddLine("call {0}", Offsets.LoL.BuyItem);
                    executor.AddLine("mov [{0}],al", memory.Address);
                    executor.AddLine("ret");
                    executor.Execute();
                }
                return Convert.ToBoolean(memory.Read<int>());
            }
        }
        public static bool SellItem(int slot)
        {
            using (var memory = new AllocatedMemory(4))
            {
                var executor = new Executor();
                lock (executor.AssemblyLock)
                {
                    executor.AddLine("mov edi,{0}", ObjectManager.Me.Inventory.BaseAddress);
                    executor.AddLine("push {0}", slot);
                    executor.AddLine("call {0}", Offsets.LoL.SellItem);
                    executor.AddLine("mov [{0}],al", memory.Address);
                    executor.AddLine("ret");
                    executor.Execute();
                }
                return Convert.ToBoolean(memory.Read<int>());
            }
        }
    }
}
