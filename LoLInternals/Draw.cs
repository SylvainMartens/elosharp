﻿using System;
using EloSharp.LoLInternals.Objects;
using EloSharp.Managers;
namespace EloSharp.LoLInternals
{
    public class Draw
    {
        public static void FloatingText(string text, GameObject gameObject, int type = 0)
        {
            try
            {
                // ReSharper disable once UnusedVariable
                IntPtr baseAddr = gameObject.BaseAddress;
            }
            catch (Exception)
            {
                Debug.Log("Tried to draw on an invalid unit.", MessageTypes.Error);
                return;
            }
            using (var memory = new AllocatedMemory(4))
            {
                var executor = new Executor();
                var floatingFont = MemoryManager.Base.Read<IntPtr>((IntPtr)Offsets.LoL.FloatingFont);
                memory.WriteString(text);
                executor.AddLine("push {0}", ObjectManager.Me.BaseAddress);
                executor.AddLine("push 0");
                executor.AddLine("push {0}", memory.Address);
                executor.AddLine("push {0}", type);
                executor.AddLine("push {0}", gameObject.BaseAddress);
                executor.AddLine("mov esi, {0}", gameObject.BaseAddress);
                executor.AddLine("xor edi, edi");
                executor.AddLine("xor ebx, ebx");
                executor.AddLine("mov ecx, {0}", floatingFont);
                executor.AddLine("mov eax, {0}", ObjectManager.Me.BaseAddress);
                executor.AddLine("call {0}", Offsets.LoL.DrawFloatText);
                executor.AddLine("ret");
                executor.Execute();
            }
        }
    }
}
