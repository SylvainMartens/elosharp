﻿using System;
using System.Windows.Forms;
using EloSharp.Forms;
namespace EloSharp
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            MainForm = new MainForm();
            Application.Run(MainForm);
        }
        public static bool Ready { get; set; }
        public static MainForm MainForm { get; set; }
    }
}