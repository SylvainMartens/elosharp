﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using GreyMagic;
namespace EloSharp
{
    class Executor : IDisposable
    {
        public object AssemblyLock = new object();
        private static readonly List<string> AssemblyList = new List<string>();
        static readonly string[] RegisterNames = { "AH", "AL", "BH", "BL", "CH", "CL", "DH", "DL", "EAX", "EBX", "ECX", "EDX" };
// ReSharper disable once UnusedMember.Local
        static readonly string[] Register32BitNames = { "EAX", "EBX", "ECX", "EDX" };
        private ExternalProcessReader Memory { get; set; }
        private IntPtr Address { get; set; }
        public Executor()
        {
            Memory = Managers.MemoryManager.LoL;
        }
        public void AddLine(string value, params object[] args)
        {
            lock (AssemblyLock)
            {
                AssemblyList.Add(string.Format(value, args));
            }
        }
        public void Execute()
        {
            try
            {
                if (!Memory.IsProcessOpen) { Debug.Log("Error: Tried to execute Assembly, but no LoL instance is open.", MessageTypes.Error); return; }
                lock (AssemblyLock)
                {
                    Memory.Asm.Clear();
                    foreach (string assemblyRow in AssemblyList)
                    {
                        InsertRandomOpCodes();
                        Memory.Asm.AddLine(assemblyRow);
                        InsertRandomOpCodes();
                    }
                    Address = Memory.AllocateMemory(Memory.Asm.Assemble().Length);
                    Memory.Asm.InjectAndExecute((uint)Address);
                    Dispose();
                }
            }
            catch (Exception ex)
            {
                Debug.Log(string.Format("[ASM] Error: {0}", ex.Message), MessageTypes.Error);
            }
        }
        public void InsertRandomOpCodes()
        {
            lock (AssemblyLock)
            {
                for (int i = 0; i <= Utils.Utils.Rand.Next(1); i++) //50%
                {
                    int rand = Utils.Utils.Rand.Next(6);
                    if (rand % 2 == 1)
                    {
                        Memory.Asm.AddLine("nop");
                    }
                    else
                    {
                        AddRandomRegisters();
                    }
                }
            }
        }
        public void AddRandomRegisters()
        {
            lock (AssemblyLock)
            {
                var ranNum = Utils.Utils.Rand.Next(0, RegisterNames.Length);
                Memory.Asm.AddLine("mov {0}, {1}", RegisterNames[ranNum], RegisterNames[ranNum]);
            }
        }
        public void Dispose()
        {
            AssemblyList.Clear();
            Memory.FreeMemory(Address);
            Memory = null;
            Thread.Sleep(25);
        }
    }
    class AllocatedMemory : IDisposable
    {
        public IntPtr Address { get; set; }
        private ExternalProcessReader Memory { get; set; }
        public AllocatedMemory(byte length)
        {
            Memory = Managers.MemoryManager.LoL;
            Address = Memory.AllocateMemory(length);
        }
        public void WriteString(string value)
        {
            Memory.WriteString(Address, value, Encoding.UTF8);
        }
        public void Write<T>(T value, int offsetInBytes = 0) where T : struct
        {
            Memory.Write(Address + offsetInBytes, value);
        }
        public T Read<T>(int offsetInBytes = 0) where T : struct
        {
            return Memory.Read<T>(Address + offsetInBytes);
        }
        public void Dispose()
        {
            Memory.FreeMemory(Address);
            Memory = null;
            Address = IntPtr.Zero;
        }
    }
}
