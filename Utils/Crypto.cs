﻿using System;
using System.Text;
namespace EloSharp.Utils
{
    class Utils
    {
        internal static Random Rand = new Random();
    }
    internal class Crypto
    {
        internal static string Encode(string str)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(str));
        }
        internal static string Decode(string str)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(str));
        }
    }
}
